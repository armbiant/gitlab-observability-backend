package gatekeeper

// Session middleware is adapted from https://github.com/boj/redistore/blob/master/redistore.go
// which provides a gin/gorilla compatible session implementation for redis.
// The redistore module is no longer maintained and does not support a redis sentinel client.

import (
	"bytes"
	"context"
	"encoding/base32"
	"encoding/gob"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	redis "github.com/go-redis/redis/v8"
	"github.com/gorilla/securecookie"
	gsessions "github.com/gorilla/sessions"
	"github.com/prometheus/client_golang/prometheus"
)

const (
	sessionKeyPrefix     = "session_"
	maxSessionLength     = 4096
	defaultMaxSessionAge = 60 * 20 // 20 minute default session length
)

type SessionOptions struct {
	CookieName      string
	CookieSecret    string
	UseSecureCookie bool
	Registry        prometheus.Registerer
}

type sessionStore struct {
	r       redis.Cmdable
	options sessions.Options
	codecs  []securecookie.Codec
}

// Get should return a cached session.
func (s *sessionStore) Get(r *http.Request, name string) (*gsessions.Session, error) {
	// Seemingly all implementations call this registry
	// see e.g. https://github.com/gorilla/sessions/blob/a6a8e49c83a2dba9cf7f486feb2662d6439851f2/store.go#L76
	return gsessions.GetRegistry(r).Get(s, name)
}

// New should create and return a new session.
//
// Note that New should never return a nil session, even in the case of
// an error if using the Registry infrastructure to cache the session.
//
// See gorilla/sessions FilesystemStore.New() for reference implementation.
func (s *sessionStore) New(r *http.Request, name string) (*gsessions.Session, error) {
	session := gsessions.NewSession(s, name)
	session.Options = s.options.ToGorillaOptions()
	session.IsNew = true
	var err error
	if c, errCookie := r.Cookie(name); errCookie == nil {
		err = securecookie.DecodeMulti(name, c.Value, &session.ID, s.codecs...)
		if err == nil {
			var ok bool
			ok, err = s.load(session)
			session.IsNew = !(err == nil && ok)
		}
	}
	return session, err
}

// Save should persist session to the underlying store implementation.
//
// If the Options.MaxAge of the session is <= 0 then the session will be
// deleted from the store. With this process it enforces session cookie
// handling so no need to trust in the cookie management in the
// web browser.
func (s *sessionStore) Save(r *http.Request, w http.ResponseWriter, session *gsessions.Session) error {
	// Marked for deletion
	if session.Options.MaxAge <= 0 {
		if err := s.delete(session); err != nil {
			return err
		}
		http.SetCookie(w, gsessions.NewCookie(session.Name(), "", session.Options))
		return nil
	}

	if session.ID == "" {
		session.ID = strings.TrimRight(base32.StdEncoding.EncodeToString(securecookie.GenerateRandomKey(32)), "=")
	}
	if err := s.save(session); err != nil {
		return err
	}
	encoded, err := securecookie.EncodeMulti(session.Name(), session.ID, s.codecs...)
	if err != nil {
		return fmt.Errorf("cookie encode: %w", err)
	}
	http.SetCookie(w, gsessions.NewCookie(session.Name(), encoded, session.Options))
	return nil
}

// Options is part of gin sessions.Store interface for passing options.
func (s *sessionStore) Options(o sessions.Options) {
	s.options = o
}

// load reads the session from redis returning true if exists.
func (s *sessionStore) load(session *gsessions.Session) (bool, error) {
	bs, err := s.r.Get(context.TODO(), sessionKeyPrefix+session.ID).Bytes()
	// key does not exist
	if errors.Is(err, redis.Nil) {
		return false, nil
	}
	if err != nil {
		return false, fmt.Errorf("redis session get: %w", err)
	}
	// empty value
	if len(bs) == 0 {
		return false, nil
	}

	if err := gob.NewDecoder(bytes.NewBuffer(bs)).Decode(&session.Values); err != nil {
		return true, fmt.Errorf("redis session decoding: %w", err)
	}
	return true, nil
}

func (s *sessionStore) save(session *gsessions.Session) error {
	buf := new(bytes.Buffer)
	if err := gob.NewEncoder(buf).Encode(session.Values); err != nil {
		return fmt.Errorf("session encode: %w", err)
	}
	bs := buf.Bytes()
	if len(bs) > maxSessionLength {
		return errors.New("session save value is too big")
	}
	age := session.Options.MaxAge
	if age == 0 {
		age = defaultMaxSessionAge
	}

	err := s.r.SetEX(context.TODO(), sessionKeyPrefix+session.ID, bs, time.Second*time.Duration(age)).Err()
	if err != nil {
		return fmt.Errorf("save session: %w", err)
	}

	return nil
}

func (s *sessionStore) delete(session *gsessions.Session) error {
	err := s.r.Del(context.TODO(), sessionKeyPrefix+session.ID).Err()
	if err != nil {
		return fmt.Errorf("session delete: %w", err)
	}
	return nil
}

func newRedisSessionStore(client redis.Cmdable, secret []byte) sessions.Store {
	return &sessionStore{
		r: client,
		// default options
		options: sessions.Options{
			Path:   "/",
			MaxAge: defaultMaxSessionAge,
		},
		codecs: securecookie.CodecsFromPairs(secret),
	}
}

// Session middleware that stores session state in redis.
func Session(client redis.Cmdable, o *SessionOptions) []gin.HandlerFunc {
	store := newRedisSessionStore(client, []byte(o.CookieSecret))

	f1 := sessions.Sessions(o.CookieName, store)
	f2 := func(ctx *gin.Context) {
		// Start a session and set the session options
		session := sessions.Default(ctx)
		session.Options(sessions.Options{
			Path:     "/",
			Domain:   "",
			MaxAge:   2592000, // 30 days
			Secure:   o.UseSecureCookie,
			HttpOnly: o.UseSecureCookie,
			// Allow UI to be embedded in iFrame
			SameSite: http.SameSiteNoneMode,
		})
	}

	return []gin.HandlerFunc{f1, f2}
}
