---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-gatekeeper-rules
spec:
  groups:
    - name: gatekeeper
      rules:
        - alert: GatekeeperAbsent
          annotations:
            title: Gatekeeper is gone
            description: Gatekeeper has dissapeared from Prometheus service discovery.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            absent(up{job="gatekeeper"} == 1)
          for: 5m
          labels:
            alertname: GatekeeperAbsent
            type: gatekeeper
            severity: s1
            alert_type: cause

        - alert: GatekeeperRedisMasterFailover
          annotations:
            title: Gatekeeper Redis Master failover
            description: Gatekeeper pods have failed over Redis Master at least once during last 15 minutes
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            sum(increase(master_failovers{job="gatekeeper"}[15m])) > 1
          for: 5m
          labels:
            alertname: GatekeeperRedisMasterFailover
            type: gatekeeper
            severity: s4
            alert_type: cause

        - alert: GatekeeperRedisMasterFlaky
          annotations:
            title: Gatekeeper Redis Master is flaky
            description: Gatekeeper pods have failed over Redis Master more than 5 times during last 15 minutes.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            sum(increase(master_failovers{job="gatekeeper"}[15m])) > 5
          for: 5m
          labels:
            alertname: GatekeeperRedisMasterFlaky
            type: gatekeeper
            severity: s2
            alert_type: cause

        - alert: GatekeeperRedisSETLatencyHigh
          annotations:
            title: Gatekeeper Redis SET latency high
            description: Gatekeeper p99 latency for SET calls to redis is higher than 3 seconds
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            histogram_quantile(0.99, sum(rate(cache_set_time_bucket{job="gatekeeper"}[5m])) by (le)) > 3
          for: 5m
          labels:
            alertname: GatekeeperRedisSETLatencyHigh
            type: gatekeeper
            severity: s3
            alert_type: cause

        - alert: GatekeeperRedisGETLatencyHigh
          annotations:
            title: Gatekeeper Redis GET latency high
            description: Gatekeeper p99 latency for GET calls to redis is higher than 3 seconds
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            histogram_quantile(0.99, sum(rate(cache_get_time_bucket{job="gatekeeper"}[5m])) by (le)) > 3
          for: 5m
          labels:
            alertname: GatekeeperRedisGETLatencyHigh
            type: gatekeeper
            severity: s3
            alert_type: cause

        - alert: GatekeeperAuthPathLatencyHigh
          annotations:
            title: Gatekeeper Auth Path latency high
            description: Gatekeeper p99 latency for authorization path /v1/auth/argus/webhook/:root_namespace_id is higher than 3 seconds
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            histogram_quantile(0.99, sum(rate(gin_request_duration_bucket{job="gatekeeper", uri="/v1/auth/argus/webhook/:root_namespace_id"}[5m])) by (le)) > 3
          for: 5m
          labels:
            alertname: GatekeeperAuthPathLatencyHigh
            type: gatekeeper
            severity: s3
            alert_type: cause

        - alert: GatekeeperTooMany500s
          annotations:
            title: Number of 5xx is high
            description: More than 3% of all requests returned 5XX
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-gatekeeper/observability-gatekeeper
          expr: |
            sum(rate(gin_uri_request_total{job="gatekeeper", code=~"5.*"}[1m])) by (uri)
              /
            sum(rate(gin_uri_request_total[1m])) by (uri)
              > .03
          for: 5m
          labels:
            alertname: GatekeeperTooMany500s
            type: gatekeeper
            severity: s2
            alert_type: cause
        # NOTE(prozlach) No 404s alert for now. ATM there are too many 404s in
        # productions compared to normal 200s to have meaningfull signal.
