variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

variable "blackbox_exporter_api_key" {
  description = "Grafana generated api key needed for the GOUI probe auth header"
  type        = string
  default     = ""
  sensitive   = true
}

variable "sentry_dsn" {
  description = "Sentry DSN url"
  type        = string
  default     = ""
  sensitive   = true
}

variable "group_error_tracking_endpoint" {
  description = "Group error tracking endpoint"
  type        = string
  default     = ""
}


variable "gatekeeper_probe_url" {
  description = "Gatekeeper probe url"
  type        = string
  default     = ""
}

variable "goui_probe_url" {
  description = "GOUI probe url"
  type        = string
  default     = ""
}

variable "environment" {
  description = "Environment. Example staging, prod etc"
  type        = string
  default     = "prod"
}
