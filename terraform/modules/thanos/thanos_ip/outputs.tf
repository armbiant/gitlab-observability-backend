output "traefik_address" {
  value       = google_compute_address.traefik-address.address
  description = "traefik IP address"
}

output "ing_address" {
  value       = google_compute_address.ing-address.address
  description = "Ingress IP address"
}