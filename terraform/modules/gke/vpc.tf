provider "google" {
  project = var.project_id
  region  = var.region
  # TODO: check if we really need to set this
  zone = var.location
}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.instance_name}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name                     = "${var.instance_name}-subnet"
  region                   = var.region
  network                  = google_compute_network.vpc.name
  private_ip_google_access = true

  ip_cidr_range = var.primary_gke_ip_cidr_range

  secondary_ip_range {
    range_name    = "pods-range"
    ip_cidr_range = var.secondary_gke_ip_cidr_range_pods
  }

  secondary_ip_range {
    range_name    = "services-ranges"
    ip_cidr_range = var.secondary_gke_ip_cidr_range_services
  }

}

resource "google_compute_router" "router" {
  name    = var.instance_name
  region  = google_compute_subnetwork.subnet.region
  network = google_compute_network.vpc.id

  bgp {
    asn = 64514
  }
}

resource "google_compute_address" "address" {
  count  = var.num_manual_ip_addresses_nat
  name   = "${var.instance_name}-nat-manual-ip-${count.index}"
  region = google_compute_subnetwork.subnet.region
}

resource "google_compute_router_nat" "nat" {
  name                   = var.instance_name
  router                 = google_compute_router.router.name
  region                 = google_compute_router.router.region
  nat_ip_allocate_option = "MANUAL_ONLY"
  nat_ips                = google_compute_address.address.*.self_link

  # default min_ports_per_vm is 64; this is to make sure terraform doesn't create a diff on every plan
  min_ports_per_vm = 64

  # Only subnetworks specific to the GKE cluster are allowed to connect via NAT.
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = google_compute_subnetwork.subnet.id
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
