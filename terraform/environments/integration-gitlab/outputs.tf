output "public_ip" {
  value       = module.gitlab.public_ip
  description = "Public IP attached to VM default network"
}

output "ssh_username" {
  value       = module.gitlab.ssh_username
  description = "SSH key username"
}

output "ssh_private_key" {
  value       = module.gitlab.ssh_private_key
  description = "SSH key used to connect to the provisioned instance"
  sensitive   = true
}
