terraform {
    source = "${get_terragrunt_dir()}/../../../..//terraform/modules/opstrace"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        vpc_id = "mock"
        vpc_name = "mock"
    }
    mock_outputs_merge_with_state = true
    mock_outputs_merge_strategy_with_state = "shallow"
    mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

dependency "sql" {
    config_path = "../sql"
    mock_outputs = {
        postgres_dsn_endpoint = "mock"
    }
    mock_outputs_merge_with_state = true
    mock_outputs_merge_strategy_with_state = "shallow"
    mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

inputs = {
    gke_cluster_name = get_env("TF_VAR_instance_name")
    ch_nodepool_nodes_number = 3
    cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
    cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
    kubeconfig_path = dependency.gke.outputs.kubeconfig_path
    postgres_dsn_endpoint = dependency.sql.outputs.postgres_dsn_endpoint
    scheduler_image = get_env("TF_VAR_scheduler_image")
}