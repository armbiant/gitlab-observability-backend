terraform {
    source = "${get_terragrunt_dir()}/../../../modules//opstracecluster"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        certmanager_service_account = "mock-sa"
        externaldns_service_account = "mock-sa"
    }
    mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

dependency "opstrace" {
    config_path = "../opstrace"
    skip_outputs = true
}

inputs = {
    cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
    cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
    kubeconfig_path = dependency.gke.outputs.kubeconfig_path
    cluster_secret_name = get_env("TF_VAR_cluster_secret_name")
    cluster_secret_namespace = get_env("TF_VAR_cluster_secret_namespace")
    gitlab_oauth_client_id = get_env("TF_VAR_oauth_client_id")
    gitlab_oauth_client_secret = get_env("TF_VAR_oauth_client_secret")
    internal_endpoint_token = get_env("TF_VAR_internal_endpoint_token")
    disable_cluster_creation = true
}
