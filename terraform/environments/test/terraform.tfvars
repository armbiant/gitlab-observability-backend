project_id = "vast-pad-240918"
# Unique name within project_id for naming resources
instance_name = "mat-dev"
# Number of nodes for Opstrace instance
num_nodes     = 3
region        = "us-west2"
location      = "us-west2-a"
zone          = "us-west2-a"

# Defaults to letsencrypt-staging for Opstrace, uncomment to change:
# cert_issuer = "letsencrypt-prod"

# Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
domain = "mat-demo.opstrace.io"
# Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
# https://letsencrypt.org/docs/expiration-emails
acme_email = "mat@opstrace.com"

# GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
gitlab_domain  = "mats-gitlab.gcp.opstrace.io"
# GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
# It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
cloud_dns_zone = "mats-gitlab-gcp"

# GitLab registry username for pulling a specific GitLab image
# Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
# test against any work in development
registry_username = "mappelman"
################ NOTE #########################
# Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
# Set this as an environment variable to prevent leaking it in SCM so instead of:
# registry_auth_token = "your_token"
# -> export TF_VAR_registry_auth_token = "your_token"

# The image to use when deploying GitLab
gitlab_image = "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:8652627bf86d02d20599b1e0caad1465bb545481"