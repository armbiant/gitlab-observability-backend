terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gitlab/gcp"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    skip_outputs = true
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
    instance_name   = local.common_vars.inputs.instance_name
    project_id      = local.common_vars.inputs.project_id
    region          = local.common_vars.inputs.region
    zone            = local.common_vars.inputs.zone
    # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
    domain          = local.common_vars.inputs.gitlab_domain
    # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
    # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
    cloud_dns_zone  = local.common_vars.inputs.cloud_dns_zone
}