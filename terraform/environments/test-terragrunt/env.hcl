locals {
    # read custom.hcl user config if exists
    custom = read_terragrunt_config("custom.hcl", {
        inputs = {}
    })
}

# inputs merge defaults and custom inputs
inputs = merge({
    # global labels to add across all our cloud resources
    gl_realm = "realm_name"
    gl_env_type = "env_type"
    gl_env_name = "env_name"
    gl_owner_email_handle = "owner_email_handle"
    gl_dept = "dept_name"
    gl_dept_group = "dept_group"

    # gcp project id
    project_id = "vast-pad-240918"
    # Unique name within project_id for naming resources
    instance_name = "abdev"
    # Number of nodes for Opstrace instance
    num_nodes = 3

    region = "us-west2"
    location = "us-west2-a"
    zone = "us-west2-a"

    # Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
    domain = "abhtngr.dev"
    # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
    gitlab_domain = "gitlab.abhtngr.dev"
    # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
    # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
    cloud_dns_zone = "abhtngr"

    # Add values for deploying cluster-specific secrets
    cluster_secret_name = "auth-secret"
    cluster_secret_namespace = "default"

    # Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
    # https://letsencrypt.org/docs/expiration-emails
    cert_issuer = "letsencrypt-staging"
    acme_server = ""
    acme_email = "abhatnagar@gitlab.com"

    # GitLab registry username for pulling a specific GitLab image
    # Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
    # test against any work in development
    registry_username = "ankitbhatnagar"

    ################ NOTE #########################
    # Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    # Set this as an environment variable to prevent leaking it in SCM so instead of:
    # registry_auth_token = "your_token"
    # -> export TF_VAR_registry_auth_token = "your_token"

    # The image to use when deploying GitLab
    gitlab_image = "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:8b5f0cba030fa01634de820d1712c2cfab04b904"
}, local.custom.inputs)