package opentelemetry

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceMonitorName(cr *v1alpha1.Group) string {
	return GetOtelDeploymentName(cr)
}

func getServiceMonitorLabels(cr *v1alpha1.Group) map[string]string {
	labels := GetOtelDeploymentSelector(cr)
	// Configure the system-tenant prometheus to scrape this
	labels["tenant"] = "system"

	return labels
}

func getServiceMonitorAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "metrics",
			Path:     "/metrics",
			Interval: "30s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Group) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: GetOtelDeploymentSelector(cr),
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Group) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(cr),
			Namespace:   cr.Namespace,
			Labels:      getServiceMonitorLabels(cr),
			Annotations: getServiceMonitorAnnotations(cr, nil),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Group, current *monitoring.ServiceMonitor) error {
	currentSpec := &current.Spec
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.OpenTelemetry.Components.ServiceMonitor.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceMonitorAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.OpenTelemetry.Components.ServiceMonitor.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(cr),
		cr.Spec.Overrides.OpenTelemetry.Components.ServiceMonitor.Labels,
	)

	return nil
}

func ServiceMonitorSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceMonitorName(cr),
	}
}
