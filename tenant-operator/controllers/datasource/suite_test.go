package datasource

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	ctrl "sigs.k8s.io/controller-runtime"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	tenantapiv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
)

var (
	k8sClient   client.Client
	testEnv     *envtest.Environment
	ctx         context.Context
	cancel      context.CancelFunc
	argusServer *httptest.Server
)

const (
	groupID = 2
)

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	SetDefaultEventuallyTimeout(time.Second * 5)
	SetDefaultEventuallyPollingInterval(time.Second)

	RunSpecs(t, "Datasource Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("../../", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
	}

	cfg, err := testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(tenantapiv1alpha1.AddToScheme(scheme.Scheme)).To(Succeed())

	// +kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	k8sManager, err := ctrl.NewManager(cfg,
		ctrl.Options{
			Scheme: scheme.Scheme,
			// disable metrics binding to prevent port clashes
			MetricsBindAddress: "0",
			Namespace:          "default",
		})
	Expect(err).NotTo(HaveOccurred())

	By("set up fake argus http server")
	argusServer = argusServerStub()

	config.Get().SetState(config.ControllerState{
		ArgusReady: true,
		AdminUrl:   argusServer.URL,
	})

	err = (&DatasourceReconciler{
		Client:   k8sManager.GetClient(),
		Scheme:   k8sManager.GetScheme(),
		Log:      ctrl.Log.WithName("controllers").WithName("datasource"),
		Recorder: k8sManager.GetEventRecorderFor("controller"),
		/* #nosec G402 */
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}).SetupWithManager(k8sManager)
	Expect(err).NotTo(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctx)
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()
})

// stub out argus endpoints for this controller
func argusServerStub() *httptest.Server {
	writeJSON := func(w http.ResponseWriter, obj interface{}) {
		if err := json.NewEncoder(w).Encode(obj); err != nil {
			panic(err)
		}
	}

	mux := &http.ServeMux{}
	mux.Handle("/api/groups/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		writeJSON(w, []argusapi.Group{
			{
				ID:   groupID,
				Name: "TWO",
			},
		})
	}))

	return httptest.NewServer(mux)
}

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	argusServer.Close()
	Expect(testEnv.Stop()).To(Succeed())
})
