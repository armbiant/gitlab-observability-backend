package argus

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func DatasourcesConfig(cr *v1alpha1.Tenant) *v1.ConfigMap {
	return &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      constants.DatasourcesConfigMapName,
			Namespace: cr.Namespace,
			Annotations: map[string]string{
				constants.LastConfigAnnotation: "",
			},
		},
	}
}

func DatasourceConfigSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.DatasourcesConfigMapName,
	}
}
