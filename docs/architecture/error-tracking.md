# Error tracking deep dive

This document provides a detailed explanation of how error tracking works.

It is assumed that the infrastructure has already been provisioned and the tenant created.
For details on how this is done, see the other docs in the `architecture` folder.

## Overview

The following diagram provides a brief overview of the components of the Gitlab Observability Platform (GOP) that implement error tracking.

![Error-tracking-overview](../assets/errortracking_overview.png)

There are two instances of error tracking API, one for a `Cluster` object, and one for each `GitlabNamespace` object.
This is because we are migrating to per-group error tracking deployments (see [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1697) and [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1737)).
The `Cluster`-level instance is launched by the `scheduler` during provisioning of the infrastructure. The `GitlabNamespace` instance is launched by the tenant `operator` during provisioning of the `GitlabNamespace` components.
The instance is chosen depending on the Sentry DSN that program uses. In turn, this is defined by the GitLab instance:

* `/errortracking/api/v1/projects/api/(\d+)/(store|envelope)` - request is handled by global instance. First capture group depicts project.
* `/v1/errortracking/(\d+)/projects/api/(\d+)/(store|envelope)` - request is handled by per-group instance. First capture group depicts group, second the project.

This API path is then matched by `ingress-nginx` and routed to the correct instance.

Both instances use ClickHouse as a backend and rely on Gatekeeper for authorization and authentication.
The error tracking component is responsible for receiving API calls, parsing payloads, and translating them to ClickHouse SQL statements.

The source of truth for authentication and authorization data is the GitLab instance.
Gatekeeper uses Redis for caching so it does not overwhelm the GitLab instance with API calls.

A user can access their project-specific Sentry DSN from the GitLab UI. They can also use the UI to view all errors generated by their application.
In turn, the UI issues API calls to the error tracking instance configured for the given Project to fetch any captured errors/events.

The instrumented code uses the Sentry SDK to issue API calls, using the provided Sentry DSN URL. These calls are then routed to the correct error tracking instance by `ingress-nginx`.
In turn, this instance performs CRUD operations on the ClickHouse database assigned to the Project.

## Workflow

The GitLab instance itself is an important component in error tracking. The instance:

* Participates in authorization of ingestion requests, acting as a source of truth for authorization and authentication.
* Provides Sentry DSN to use in instrumented program, which is the ingestion URL, plus authentication and authorization data.
* Presents results in the UI. This may be replaced by GitLab Observability UI in the future. See [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1876) for more details.

On the other side are the error tracking components which are part of the Gitlab Observability Platform (GOP).
The communication between these two components is done through an API.

### Error tracking API

The error tracking API is OpenAPI compliant. It is defined using Swagger, which helps autogenerate much of the boilerplate code for the underlying API and corresponding routes.
The schema definition can be found in the `go/pkg/errortracking/swagger.yaml` file in this repository.

From the perspective of error ingestion, there are two significant endpoints:

* `/projects/api/{projectId}/envelope` - see [here](https://develop.sentry.dev/sdk/envelopes/) for more details.
* `/projects/api/{projectId}/store` - see [here](https://develop.sentry.dev/sdk/store/) for more details.

They differ in the semantics of the payload and the capabilities.
Error tracking code is currently unable to parse all the fields that Sentry SDK sends in the payload.
For example, only basic support is provided with `capture_exception` as the holding method.
Additional features requests (see this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/340178)) will be added on a case-by-case basis.

Refer to the example programs listed in `docs/guides/user/error_tracking.md` for details on how to enable debugging when sending errors.
This way, you can preview the payload in JSON format before it is sent to GOP.

Remaining API paths, such as those below, are used by GitLab to present and manage captured errors and the information attached to them:

* `/projects/{projectId}/errors` - listing errors.
* `/projects/{projectId}/errors/{fingerprint}` - get information about the error.

### Authentication and Authorization

Depending on whether you are ingesting errors or listing/reading errors stored in GOP, there are two different authentication and authorization paths:

* Error ingestion path
* Error management/listing path

#### Error ingestion path

Both `Cluster`-level, and `GitlabNamespace`-level error tracking ingress objects are annotated with `nginx.ingress.kubernetes.io/auth-url` annotation, which points to the Gatekeeper service:

```go
gatekeeperURL := fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001/v1/error_tracking/auth", cr.Namespace())
```

This leverages the `ngx_http_auth_request_module` [NGINX module](https://nginx.org/en/docs/http/ngx_http_auth_request_module.html) to provide authentication and authorization to error tracking APIs.
The request, before it's forwarded to the error tracking API instance, is forwarded to the Gatekeeper:

* If the subrequest returns a 2xx response code, access is allowed.
* If the subrequest returns a 401 or 403 response code, access is denied.
* Any other response code returned by the subrequest is considered an error.

After receiving the auth request, the Gatekeeper checks whether error tracking was enabled for the given auth key:

```go
key := fmt.Sprintf("%s-%s", sentryKey, projectID)
```

The Gatekeeper first checks the Redis cache (which also has a local in-memory layer). If the entry is not found, the Gatekeeper queries the GitLab instance.
The reply from the GitLab instance is then stored in the cache and returned to `ingress-nginx`.

```go
func (h *errorTrackingAuthHandler) handleIngestRequest(ctx *gin.Context) {
    sentryKey, err := extractSentryKey(ctx)
    if err != nil {
        ctx.String(401, "Unauthorized")
        return
    }

    projectID, err := extractProjectID(ctx)
    if err != nil {
        ctx.String(401, "Unauthorized")
        return
    }

    c := GetCache(ctx)
    key := fmt.Sprintf("%s-%s", sentryKey, projectID)
    var enabled bool

    err = c.Get(ctx, key, &enabled)
    if err != nil {
        m := h.store[key]
        if m == nil {
            m = &sync.Mutex{}
            h.store[key] = m
        }
        m.Lock()
        defer m.Unlock()

        err = c.Get(ctx, key, &enabled)
        if err != nil {
            enabled, err = checkIfKeyIsEnabled(ctx, sentryKey, projectID)
            if err != nil {
                ctx.String(401, "Unauthorized")
                return
            }
            c.Set(&cache.Item{
                Ctx:   ctx,
                Key:   key,
                Value: enabled,
                TTL: 2 * time.Minute,
            })
        }
    }

    if !enabled {
        ctx.String(401, "Unauthorized")
        return
    }

    ctx.String(200, "Success")
}
```

##### Error management/listing path

The workflow is much simpler in this path.
First, the code checks whether the request contains a `Gitlab-Error-Tracking-Token` header.
If so, its value is compared with a pre-shared key that is stored in a secret and then injected into Gatekeeper deployment as an environment variable.
The field for this secret is `internal_endpoint_token`.
The request is permitted only if the header is found and its value matches the key.

```go
type OpenAPIAuthHeader struct {
    Token string `header:"Gitlab-Error-Tracking-Token" binding:"required"`
}

func (h *errorTrackingAuthHandler) handleReadRequest(ctx *gin.Context) {
    var authHeader OpenAPIAuthHeader

    err := ctx.ShouldBindHeader(&authHeader)
    if err != nil {
        ctx.String(401, "Unauthorized")
        return
    }

    cfg := GetConfig(ctx)
    if authHeader.Token != cfg.GitlabInternalEndpointToken {
        ctx.String(401, "Unauthorized")
        return
    }

    ctx.String(200, "Success")
}
```
