import { b64encode } from 'k6/encoding'

const ERROR_TRACKING_API = __ENV.ERROR_TRACKING_API
const PROJECT_ID = __ENV.PROJECT_ID
const SENTRY_SECRET = __ENV.SENTRY_SECRET

export default {
    urls: {
        store: `${ERROR_TRACKING_API}/projects/api/${PROJECT_ID}/store?sentry_key=${SENTRY_SECRET}`
    },
    headers: {
        default: {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${b64encode(SENTRY_SECRET + ':')}`,
        },
        store: {
            'Content-Type': 'application/json',
        },
    }
}
