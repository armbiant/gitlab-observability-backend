# CI config for ClickHouse operator, this is included into the top level CI yml.
# Note(joe): keeping separate from the main CI file as we expect this workspace will become its own project in time.

variables:
  DOCKER_VERSION: "20.10.14"
  GO_VERSION: "1.19.3"
  E2E_IMAGE: $CI_REGISTRY_IMAGE/ch-operator-e2e:0.0.2

.ch-operator-merge-request: &ch-operator-mr
  if: $CI_PIPELINE_SOURCE == "merge_request_event"
  changes: [clickhouse-operator/**/*]

.ch-docker-base:
  image: docker:${DOCKER_VERSION}-git
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  rules:
    - <<: *ch-operator-mr
  tags:
    - gitlab-org-docker
  before_script:
    - apk add --no-cache make
    - echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - cd clickhouse-operator

ch-operator e2e image:
  stage: images
  extends:
    - .ch-docker-base
  rules:
    - <<: *ch-operator-mr
      changes:
        - clickhouse-operator/ci/e2e/Dockerfile
  script:
    - docker build -f ci/e2e/Dockerfile --build-arg GO_VERSION=$GO_VERSION -t "$E2E_IMAGE" .
    - docker push "$E2E_IMAGE"

.ch-e2e-base:
  stage: e2e
  image: $E2E_IMAGE
  rules:
    - <<: *ch-operator-mr
  before_script:
    - cd clickhouse-operator
    - gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS} --project ${OPSTRACE_DEV_GCP_PROJECT_ID}
  variables:
    # use a "low carbon" zone
    ZONE: us-central1-a
    CLUSTER: ch-operator-e2e-${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_IID}

ch-operator e2e:
  extends:
    - .ch-e2e-base
  script:
    - >
      gcloud container clusters create "$CLUSTER" --zone=${ZONE}
      --disk-size=50GB --disk-type=pd-standard --num-nodes=3 --machine-type=n1-standard-4 --quiet
    - gcloud container clusters get-credentials "$CLUSTER" --zone=${ZONE}
    - make install
    - make deploy-dev-dependencies
    - make deploy
    - make e2e-test

ch-operator e2e cleanup:
  extends:
    - .ch-e2e-base
  # don't interrupt cleanup as it keeps costs down
  interruptible: false
  # allow to fail, the cluster might not have been created successfully
  allow_failure: true
  script:
    - gcloud container clusters delete "$CLUSTER" --zone=${ZONE} --quiet
  needs:
    - ch-operator e2e
  when: always
