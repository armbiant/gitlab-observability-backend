package types

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	SupportedTypeException = "exception"
	SupportedTypeMessage   = "message"
	UnsupportedDataItem    = "unknown"
	SessionType            = "session"
	EventType              = "event"
)

type EnvelopeMetadata struct {
	EventID string    `json:"event_id"`
	SentAt  time.Time `json:"sent_at"`
}

type EnvelopeType struct {
	Type   string `json:"type"`
	Length int    `json:"length"`
}

type Frame struct {
	Function    string                 `json:"function,omitempty"`
	Symbol      string                 `json:"symbol,omitempty"`
	Module      string                 `json:"module,omitempty"`
	Package     string                 `json:"package,omitempty"`
	Filename    string                 `json:"filename,omitempty"`
	AbsPath     string                 `json:"abs_path,omitempty"`
	Lineno      int                    `json:"lineno,omitempty"`
	Colno       int                    `json:"colno,omitempty"`
	PreContext  []string               `json:"pre_context,omitempty"`
	ContextLine string                 `json:"context_line,omitempty"`
	PostContext []string               `json:"post_context,omitempty"`
	InApp       bool                   `json:"in_app,omitempty"`
	Vars        map[string]interface{} `json:"vars,omitempty"`
}

type Stacktrace struct {
	Frames        []Frame `json:"frames,omitempty"`
	FramesOmitted []uint  `json:"frames_omitted,omitempty"`
}

// Exception holds core attributes. See https://develop.sentry.dev/sdk/event-payloads/exception/#attributes.
type Exception struct {
	Type       string      `json:"type,omitempty"`
	Value      string      `json:"value,omitempty"`
	Module     string      `json:"module,omitempty"`
	ThreadID   interface{} `json:"thread_id,omitempty"`
	Stacktrace *Stacktrace `json:"stacktrace,omitempty"`
}

type Span struct {
	TraceID      string                 `json:"trace_id"`
	SpanID       string                 `json:"span_id"`
	ParentSpanID string                 `json:"parent_span_id"`
	Op           string                 `json:"op,omitempty"`
	Description  string                 `json:"description,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Tags         map[string]string      `json:"tags,omitempty"`
	StartTime    time.Time              `json:"start_timestamp"`
	EndTime      time.Time              `json:"timestamp"`
	Data         map[string]interface{} `json:"data,omitempty"`
}

// MessageObj is representation of https://develop.sentry.dev/sdk/event-payloads/message/.
type MessageObj struct {
	Formatted string `json:"formatted"`
}

// Event is a representation of https://develop.sentry.dev/sdk/event-payloads/.
// Relevant schema on Gitlab App https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json
//
//nolint:lll
type Event struct {
	Dist        string    `json:"dist,omitempty"`
	Environment string    `json:"environment,omitempty"`
	EventID     string    `json:"event_id,omitempty"`
	Level       string    `json:"level,omitempty"`
	Message     string    `json:"message,omitempty"`
	Platform    string    `json:"platform,omitempty"`
	Release     string    `json:"release,omitempty"`
	Timestamp   time.Time `json:"timestamp"`
	Transaction string    `json:"transaction,omitempty"`
	// Exception can be an object with the attribute values [] or a flat list of objects.
	// See https://develop.sentry.dev/sdk/event-payloads/exception/.
	// This is field is decoded in a special way
	Exception []*Exception `json:"-"`
	// The fields below are only relevant for transactions.
	Type      string    `json:"type,omitempty"`
	StartTime time.Time `json:"start_timestamp"`
	Spans     []*Span   `json:"spans,omitempty"`
	// This will hold a pointer to  the first exception that has a stacktrace
	// since the first exception may not provide adequate context (e.g. in the
	// Go SDK).
	exception *Exception `json:"-"`

	ServerName string `json:"server_name,omitempty"`
}

// DateTimeFormats is the list of supported error event timestamp formats.
var DateTimeFormats = []string{
	time.RFC3339,
	"2006-01-02T15:04:05",
}

// ExceptionValues holds the exception in the values field.
// See https://develop.sentry.dev/sdk/event-payloads/exception/.
type ExceptionValues struct {
	Values []*Exception `json:"values"`
}

type Session struct {
	SessionID  string            `json:"sid,omitempty"`
	UserID     string            `json:"did,omitempty"`
	Init       uint8             `json:"init,omitempty"`
	Started    time.Time         `json:"started,omitempty"`
	OccuredAt  time.Time         `json:"timestamp,omitempty"`
	Duration   float64           `json:"duration,omitempty"`
	Status     string            `json:"status,omitempty"`
	Attributes SessionAttributes `json:"attrs,omitempty"`
}

type SessionAttributes struct {
	Release     string `json:"release,omitempty"`
	Environment string `json:"environment,omitempty"`
}

// Use a map for finding keys fast.
var statusValues = map[string]uint8{
	"ok":       uint8(1),
	"exited":   uint8(2),
	"crashed":  uint8(3),
	"abnormal": uint8(4),
}

func (s *Session) ValidateStatus() error {
	_, ok := statusValues[s.Status]
	if ok {
		return nil
	}
	return fmt.Errorf("session status unknown: %s", s.Status)
}

func (s *Session) Validate() error {
	if s.SessionID == "" || s.Attributes.Release == "" {
		return fmt.Errorf("validation failed: session is missing required filed Release or SessionID")
	}

	if err := s.ValidateStatus(); err != nil {
		return fmt.Errorf("validation failed: %w", err)
	}

	return nil
}

//nolint:cyclop
func (s *Session) UnmarshalJSON(data []byte) error {
	var objMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event into objMap: %w", err)
	}

	for key, val := range objMap {
		switch key {
		case "sid":
			err = json.Unmarshal(*val, &s.SessionID)
		case "did":
			// Rust Sentry SDK returns nil instead of empty string
			if val != nil {
				err = json.Unmarshal(*val, &s.UserID)
			}
		case "init":
			var init bool
			err = json.Unmarshal(*val, &init)
			if init {
				s.Init = uint8(1)
			} else {
				s.Init = uint8(0)
			}
		case "duration":
			err = json.Unmarshal(*val, &s.Duration)
		case "status":
			err = json.Unmarshal(*val, &s.Status)
		case "started":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			s.Started = ts.Time()
		case "timestamp":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			s.OccuredAt = ts.Time()
		case "attrs":
			err = json.Unmarshal(*val, &s.Attributes)
		}
		if err != nil {
			//nolint:wrapcheck
			return err
		}
	}

	// Some SDKs do not set the timestamp field (OccuredAt)
	if s.OccuredAt.IsZero() {
		s.OccuredAt = time.Now().UTC()
	}

	return nil
}

// UnixOrRFC3339Time is a helper struct to parse a timestamp sent by the sentry sdk.
// Sentry sdk can inconsistently send timestamps as unix timestamp in milliseconds or
// RFC3339 (ISO 8601) timestamp.
// See https://develop.sentry.dev/sdk/event-payloads/#required-attributes (timestamp).
type UnixOrRFC3339Time struct {
	t time.Time
}

// UnmarshalJSON loads either an RFC3339-formatted time (e.g.
// '"2020-11-03T15:15:09Z"') or unix epoch timestamp (e.g. '1498827360.000').
// However few sdks are missing full RFC3339 format so handles those as well.
func (u *UnixOrRFC3339Time) UnmarshalJSON(data []byte) error {
	var (
		placeholder interface{}
		lastErr     error
	)
	lastErr = json.Unmarshal(data, &placeholder)
	if lastErr != nil {
		return fmt.Errorf("failed to unmarshal timestamp: %w", lastErr)
	}
	switch val := placeholder.(type) {
	case string:
		for _, layout := range DateTimeFormats {
			parsed, err := time.Parse(layout, val)
			if err == nil {
				u.t = parsed
				return nil
			}
			lastErr = err
		}
		if lastErr != nil {
			return fmt.Errorf("failed to parse timestamp: %w", lastErr)
		}
	case float64:
		f, err := strconv.ParseFloat(string(data), 64)
		if err != nil {
			return fmt.Errorf("failed to parse timestamp as float: %w", err)
		}

		i := int64(f * 1000)

		u.t = time.Unix(0, i*int64(time.Millisecond))
	}

	return nil
}

func (u *UnixOrRFC3339Time) Time() time.Time {
	return u.t
}

// UnmarshalJSON parses the event payload and validates if the required attributes are present.
// For ref: see parsing in Gitlab app: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/sentry_request_parser.rb
// and validation: https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/lib/error_tracking/collector/payload_validator.rb
//
//nolint:lll,gocyclo,cyclop,funlen
func (e *Event) UnmarshalJSON(data []byte) error {
	var objMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event into objMap: %w", err)
	}

	for key, val := range objMap {
		switch key {
		case "dist":
			err = json.Unmarshal(*val, &e.Dist)
		case "environment":
			err = json.Unmarshal(*val, &e.Environment)
		case "event_id":
			err = json.Unmarshal(*val, &e.EventID)
		case "level":
			err = json.Unmarshal(*val, &e.Level)
		case "platform":
			err = json.Unmarshal(*val, &e.Platform)
		case "transaction":
			err = json.Unmarshal(*val, &e.Transaction)
		case "release":
			err = json.Unmarshal(*val, &e.Release)
		case "server_name":
			err = json.Unmarshal(*val, &e.ServerName)
		case "message":
			// message can be an object or a string. See https://develop.sentry.dev/sdk/event-payloads/message/.
			// Remove leading whitespace to correctly identify if the value is an object
			messageBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(messageBts) > 0 && messageBts[0] == '{'
			if isObject {
				var obj = &MessageObj{}
				err = json.Unmarshal(*val, obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal message into MessageObj: %w", err)
				}
				e.Message = obj.Formatted
			} else {
				err = json.Unmarshal(*val, &e.Message)
			}
		case "timestamp":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			e.Timestamp = ts.Time()
		case "exception":
			// exception can be an object or an array. See https://develop.sentry.dev/sdk/event-payloads/exception/.
			// Remove leading whitespace to correctly identify it.
			exceptionBts := bytes.TrimLeft(*val, " \t\r\n")
			isObject := len(exceptionBts) > 0 && exceptionBts[0] == '{'
			isArray := len(exceptionBts) > 0 && exceptionBts[0] == '['

			if isArray {
				// directly unmarshall into []Exception values
				err = json.Unmarshal(*val, &e.Exception)
			} else if isObject {
				var obj = ExceptionValues{}
				err = json.Unmarshal(*val, &obj)
				if err != nil {
					return fmt.Errorf("failed to unmarshal exception into ExceptionValues: %w", err)
				}
				e.Exception = obj.Values
			}
		}
		if err != nil {
			//nolint:wrapcheck
			return err
		}
	}
	// In case where `timestamp` key is missing in the payload, fill it on server side
	if e.Timestamp.IsZero() {
		log.Info("missing 'timestamp' column in exception event payload, filling it on server")
		e.Timestamp = time.Now().UTC()
	}

	// Find pointer to the first exception that has a stacktrace since the first
	// exception may not provide adequate context (e.g. in the Go SDK).
	// Original ruby code:
	//nolint:lll
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L41-48
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L56-67
	for i, ex := range e.Exception {
		if ex.Stacktrace == nil {
			continue
		}
		e.exception = e.Exception[i]
		break
	}

	return nil
}

func (e *Event) ExtractDataItemType() string {
	if e.exception != nil {
		return SupportedTypeException
	}
	if e.Message != "" {
		return SupportedTypeMessage
	}
	return UnsupportedDataItem
}

func (e *Event) ValidateException() error {
	if e.exception == nil {
		return fmt.Errorf("invalid error event of exception type; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event of exception type: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event of exception type: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event of exception type: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event of exception type: platform is not set")
	}

	return nil
}

// Actor for message returns first item in stacktrace that has a function and module name
// if stacktraces are not available then an empty string is returned.
// TODO: We need to implement message actor by parsing the stacktrace for various SDKs
// for now we will not be handling stacktraces

func (e Event) MessageActor(payload []byte) string {
	return ""
}

func (e *Event) ValidateMessage() error {
	if e.Platform == "" {
		return fmt.Errorf("invalid error event of message type: platform is not set")
	}

	if e.Message == "" {
		return fmt.Errorf("invalid error event of message type: message is not set")
	}
	return nil
}

// Validate Event has required fields set.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/error_tracking/error.rb#L20-25
func (e *Event) Validate() error {
	// Note(Arun): Currently exception is a mandatory field as per our prior implementation.
	//nolint:lll
	// See https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json#L4 for the list of required fields.
	if e.exception == nil {
		return fmt.Errorf("invalid error event; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event: platform is not set")
	}

	return nil
}

// Actor returns the transaction name. If the error does not have one set it
// returns the first item in stacktrace that has a function and module name.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L11
// - https://gitlab.com/gitlab-org/gitlab/-/blob/daa5796df89d775c635dc900a532efb657877b75/app/services/error_tracking/collect_error_service.rb#L66
//
//nolint:lll
func (e Event) ExceptionActor() string {
	if e.Transaction != "" {
		return e.Transaction
	}

	// If no exception is present return early
	if e.exception == nil {
		return ""
	}
	// This means there's no actor name and we should fail.
	l := len(e.exception.Stacktrace.Frames)
	if l == 0 {
		return ""
	}

	// Some SDKs do not have a transaction attribute. So we build it by
	// combining function name and module name from the last item in stacktrace.
	//nolint:lll
	// https://gitlab.com/gitlab-org/gitlab/-/blob/8565b9d4770baafa560915b6b06da6026ecab41c/app/services/error_tracking/collect_error_service.rb#L62

	f := e.exception.Stacktrace.Frames[l-1]
	return fmt.Sprintf("%s(%s)", f.Function, f.Module)
}

// Name returns the exception type.
// Original ruby code:
// https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L9
//
//nolint:lll
func (e Event) Name() string {
	if e.exception != nil {
		return e.exception.Type
	}
	return ""
}

// Description returns the exception value.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L10
//
//nolint:lll
func (e Event) Description() string {
	if e.exception != nil {
		return e.exception.Value
	}
	return ""
}

type Envelope struct {
	Metadata *EnvelopeMetadata
	Type     *EnvelopeType
	Event    *Event
	Session  *Session
}

type envelopeScanner struct {
	s *bufio.Reader
}

func (e *envelopeScanner) do(v interface{}) error {
	payload, isPrefix, err := e.s.ReadLine()
	// Quoting from docs:
	// If the line was too long for the buffer then isPrefix is set and the beginning of the line is returned.
	if isPrefix {
		return fmt.Errorf("envelope payload too large( > 1MB)")
	}
	if err != nil {
		return fmt.Errorf("failed to read payload: %w", err)
	}
	err = json.Unmarshal(payload, v)
	if err != nil {
		return fmt.Errorf("failed to unmarshal payload key: %w", err)
	}
	return nil
}

// Some envelope sizes can be bigger, we clamp it down at 1 MB.
const maxPayloadSize = 1024 * 1024

// NewEnvelopeFrom expects a payload request containing 3 objects: sentry
// metadata, type data and event data. It parses the payload and returns an
// Envelope object or an error.
//
// For reference, this is how the upstream sentry-go client sdk encodes the
// payload:
// https://github.com/getsentry/sentry-go/blob/409df0940aada10321428286de0c0b59fde0a796/transport.go#L96
func NewEnvelopeFrom(payload []byte) (*Envelope, error) {
	e := &Envelope{
		Metadata: &EnvelopeMetadata{},
		Type:     &EnvelopeType{},
		Event:    &Event{},
		Session:  &Session{},
	}

	r := bytes.NewReader(payload)
	s := &envelopeScanner{
		s: bufio.NewReaderSize(r, maxPayloadSize),
	}

	e.Metadata = &EnvelopeMetadata{}
	e.Type = &EnvelopeType{}
	e.Event = &Event{}
	e.Session = &Session{}

	err := s.do(e.Metadata)
	if err != nil {
		return nil, err
	}
	err = s.do(e.Type)
	if err != nil {
		return nil, err
	}

	switch e.Type.Type {
	case EventType:
		err = s.do(e.Event)
		if err != nil {
			return nil, err
		}
		return e, nil
	case SessionType:
		err = s.do(e.Session)
		if err != nil {
			return nil, err
		}
		return e, nil
	default:
		return nil, fmt.Errorf("envelope type %s cannot be processed", e.Type.Type)
	}
}

// NewEventFrom parses a payload request to an Event object.
// Note: Currently we don't have limits on payload size when ingesting via this method.
func NewEventFrom(payload []byte) (*Event, error) {
	e := &Event{}

	err := e.UnmarshalJSON(payload)
	return e, err
}
