package errortracking

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

const (
	PayloadTypeEnvelope int = iota
	PayloadTypeEvent
)

// When client-side buffering is enabled, we only buffer the requested
// payload post validation. All processing is then delegated to the
// queue consuming process to keep the write paths as performant as
// possible.
type QueueItem struct {
	Type      int    `json:"type"`
	Payload   []byte `json:"payload"`
	ProjectID uint64 `json:"projectID"`
}

func (c *Controller) QueueProcessor(buffer []interface{}) error {
	log.Debugf("calling queue processing with %d records", len(buffer))
	if len(buffer) == 0 {
		return nil // nothing to do
	}

	if err := c.processQueueItems(buffer); err != nil {
		log.WithError(err).Warn("processing events")
		return err
	}

	return nil
}

func (c *Controller) processQueueItems(buffer []interface{}) error {
	conn, err := c.db.GetConn()
	if err != nil {
		return fmt.Errorf("getting database connection: %w", err)
	}

	ctx := context.Background()
	timeoutContext, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	batches, err := newEventsBatchPackage(timeoutContext, conn)
	if err != nil {
		return fmt.Errorf("creating an events batch package: %w", err)
	}

	for _, d := range buffer {
		// parses raw bytes into queue items
		item, err := parsePayload(d)
		if err != nil {
			log.WithError(err).Warn("parsing payload")
			continue
		}

		// Parses queue items into sentry data items.
		// A sentry data item is just a placeholder of holding any data item
		// from the Sentry universe
		sentryDataItem, err := parseQueueItem(item)
		if err != nil {
			log.WithError(err).Warn("parsing queue item")
			continue
		}

		// get a sentry data item manager that knows how to handle each data item type
		manager, err := getSentryDataItemManager(sentryDataItem)
		if err != nil {
			log.Warn(err)
		}
		err = manager.Handle(item.ProjectID, sentryDataItem, item.Payload, &batches)
		if err != nil {
			log.Warn(err)
		}
	}

	batches.Send()
	return nil
}

func getSentryDataItemManager(d *SentryDataItem) (ISentryDataItemHandler, error) {
	switch d.Type {
	case types.EventType:
		// in case of an event we need to figure out if it is an error event or message event
		event, ok := d.DataItem.(*types.Event)
		if !ok {
			return nil, fmt.Errorf("could not convert sentry data item to event while getting SentryDataItemManager")
		}
		switch event.ExtractDataItemType() {
		case types.SupportedTypeException:
			return &EventErrorHandler{}, nil
		case types.SupportedTypeMessage:
			return &EventMessageHandler{}, nil
		default:
			return nil, fmt.Errorf("unknown event type, cannot get a SentryDataItemManager")
		}
	case types.SessionType:
		return &SessionHandler{}, nil
	default:
		return nil, fmt.Errorf("unknown sentry data item type, cannot get a SentryDataItemManager")
	}
}

// SentryDataItem can hold an event (exception or message) or a session
// In the future it can hold more sentry data items like transactions etc.
type SentryDataItem struct {
	DataItem interface{}
	Type     string
}

func NewSentryDataItem(dataItemType string) (*SentryDataItem, error) {
	switch dataItemType {
	case types.EventType:
		return &SentryDataItem{
			DataItem: types.Event{},
			Type:     dataItemType,
		}, nil
	case types.SessionType:
		return &SentryDataItem{
			DataItem: types.Session{},
			Type:     dataItemType,
		}, nil
	default:
		return nil, fmt.Errorf("could not initialize a new SentryDataItem")
	}
}

type ISentryDataItemHandler interface {
	Handle(projectID uint64, dataItem *SentryDataItem, payload []byte, b *EventsBatchPackage) error
}

type EventMessageHandler struct{}

func (me *EventMessageHandler) Handle(
	projectID uint64, dataItem *SentryDataItem, payload []byte, b *EventsBatchPackage) error {
	message, ok := (dataItem.DataItem).(*types.Event)
	if !ok {
		return fmt.Errorf("could not cast sentry data item into event")
	}

	m := NewErrorTrackingMessageEvent(projectID, message, payload)
	if err := b.MessageEventsBatch.Add(buildMessageCompnents(m)...); err != nil {
		return fmt.Errorf("adding to messages batch: %w", err)
	}
	return nil
}

type EventErrorHandler struct{}

func (e *EventErrorHandler) Handle(
	projectID uint64, dataItem *SentryDataItem, payload []byte, b *EventsBatchPackage) error {
	event, ok := (dataItem.DataItem).(*types.Event)
	if !ok {
		return fmt.Errorf("could not cast sentry data item into event")
	}

	// batch error v1
	v1 := NewErrorTrackingErrorEvent(projectID, event, payload)
	if err := b.ErrorV1EventsBatch.Add(buildEventComponents(v1)...); err != nil {
		return fmt.Errorf("adding to batch error event v2: %w", err)
	}

	if err := b.ErrorV1StatusBatch.Add(buildStatusComponents(v1)...); err != nil {
		return fmt.Errorf("adding to batch status v1: %w", err)
	}

	// batch error v2
	v2, err := NewErrorEventV2(projectID, event, payload)
	if err != nil {
		return fmt.Errorf("creating error event v2: %w", err)
	}

	if err := b.ErrorV2EventsBatch.Add(buildEventV2Components(v2)...); err != nil {
		return fmt.Errorf("adding to batch error event v2: %w", err)
	}

	if err := b.ErrorV2StatusBatch.Add(buildStatusV2Components(v2)...); err != nil {
		return fmt.Errorf("adding to batch status v2: %w", err)
	}

	return nil
}

type SessionHandler struct{}

func (s *SessionHandler) Handle(
	projectID uint64, dataItem *SentryDataItem, payload []byte, b *EventsBatchPackage) error {
	session, ok := (dataItem.DataItem).(*types.Session)
	if !ok {
		return fmt.Errorf("could not cast sentry data item into session")
	}

	se := NewErrorTrackingSession(projectID, session, payload)
	if err := b.SessionsBatch.Add(buildSessionsCompnents(se)...); err != nil {
		return fmt.Errorf("adding to sessions batch: %w", err)
	}
	return nil
}

type EventsBatchPackage struct {
	ErrorV1EventsBatch BatchPackage
	ErrorV1StatusBatch BatchPackage
	ErrorV2EventsBatch BatchPackage
	ErrorV2StatusBatch BatchPackage
	MessageEventsBatch BatchPackage
	SessionsBatch      BatchPackage
}

func (ebp *EventsBatchPackage) Send() {
	send(ebp.ErrorV1EventsBatch.Batch, ebp.ErrorV1EventsBatch.Count)
	send(ebp.ErrorV1StatusBatch.Batch, ebp.ErrorV1StatusBatch.Count)
	send(ebp.ErrorV2EventsBatch.Batch, ebp.ErrorV2EventsBatch.Count)
	send(ebp.ErrorV2StatusBatch.Batch, ebp.ErrorV2StatusBatch.Count)
	send(ebp.MessageEventsBatch.Batch, ebp.MessageEventsBatch.Count)
	send(ebp.SessionsBatch.Batch, ebp.SessionsBatch.Count)
}

type BatchPackage struct {
	Batch driver.Batch
	Count int64
}

func (b *BatchPackage) Add(data ...interface{}) error {
	if err := b.Batch.Append(data...); err != nil {
		return fmt.Errorf("creating appending data to batch: %w", err)
	}
	b.Count++
	return nil
}

func newEventsBatchPackage(timeoutContext context.Context, conn *driver.Conn) (EventsBatchPackage, error) {
	var err error

	p := EventsBatchPackage{}

	p.ErrorV1EventsBatch.Count = 0
	p.ErrorV1EventsBatch.Batch, err = (*conn).PrepareBatch(timeoutContext, "INSERT INTO error_tracking_error_events")
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating error events v1 batch: %w", err)
	}
	p.ErrorV1StatusBatch.Count = 0
	p.ErrorV1StatusBatch.Batch, err = (*conn).PrepareBatch(timeoutContext, "INSERT INTO error_tracking_error_status")
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating error status v1 batch: %w", err)
	}

	p.ErrorV2EventsBatch.Count = 0
	p.ErrorV2EventsBatch.Batch, err = (*conn).PrepareBatch(timeoutContext,
		`INSERT INTO error_tracking_events_v2
		 (
			event_id,
			project_id,
			timestamp,
			is_deleted,
			fingerprint,
			actor,
			platform,
			environment,
			level,
			payload,
			message,
			transaction
		)`)
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating error events v2 batch: %w", err)
	}

	p.ErrorV2StatusBatch.Count = 0
	p.ErrorV2StatusBatch.Batch, err = (*conn).PrepareBatch(timeoutContext,
		`INSERT INTO error_tracking_error_status_v2
		(
			event_id,
			project_id,
			timestamp,
			is_deleted,
			fingerprint,
			status,
			actor,
			updated_by,
			updated_at
		)`)
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating error status v2 batch: %w", err)
	}

	p.MessageEventsBatch.Count = 0
	p.MessageEventsBatch.Batch, err = (*conn).PrepareBatch(timeoutContext,
		`INSERT INTO error_tracking_message_events
		(
			event_id,
			project_id,
			timestamp,
			is_deleted,
			fingerprint,
			environment,
			level,
			message,
			actor,
			platform,
			release,
			server_name,
			payload
		)`)
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating message event batch: %w", err)
	}

	p.SessionsBatch.Count = 0
	p.SessionsBatch.Batch, err = (*conn).PrepareBatch(timeoutContext,
		`INSERT INTO error_tracking_sessions
		(
			project_id,
			session_id,
			user_id,
			init,
			payload,
			started,
			occurred_at,
			duration,
			status,
			release,
			environment
		)`)
	if err != nil {
		return EventsBatchPackage{}, fmt.Errorf("creating session batch: %w", err)
	}

	return p, nil
}

// returns a queue item that can be of envelope or pure payload type.
func parsePayload(d interface{}) (*QueueItem, error) {
	dBytes, ok := d.([]byte)
	if !ok {
		return nil, fmt.Errorf("received item is not a byte slice, cannot unmarshall")
	}

	var item QueueItem
	if err := json.Unmarshal(dBytes, &item); err != nil {
		return nil, fmt.Errorf("unmarshalling received item: %w", err)
	}

	return &item, nil
}

//nolint:wrapcheck
func parseQueueItem(item *QueueItem) (*SentryDataItem, error) {
	switch item.Type {
	case PayloadTypeEnvelope:
		e, err := types.NewEnvelopeFrom(item.Payload)
		if err != nil {
			return nil, err
		}
		return transformEnvelopeToSentryDataItem(e)
	case PayloadTypeEvent:
		// Payload type event contain only events. No sessions
		e, err := types.NewEventFrom(item.Payload)
		if err != nil {
			return nil, err
		}

		data, err := NewSentryDataItem(types.EventType)
		if err != nil {
			return nil, err
		}
		data.DataItem = e
		return data, nil
	default:
		return nil, fmt.Errorf("cannot process sentry data item")
	}
}

func transformEnvelopeToSentryDataItem(envelope *types.Envelope) (*SentryDataItem, error) {
	switch envelope.Type.Type {
	case types.EventType:
		if envelope.Event == nil {
			return nil, fmt.Errorf("could not transform envelope event type in to Sentry data item")
		}
		d, err := NewSentryDataItem(envelope.Type.Type)
		if err != nil {
			return nil, err
		}
		d.DataItem = envelope.Event
		return d, nil
	case types.SessionType:
		if envelope.Session == nil {
			return nil, fmt.Errorf("could not transform envelope session type in to Sentry data item")
		}
		d, err := NewSentryDataItem(envelope.Type.Type)
		if err != nil {
			return nil, err
		}
		d.DataItem = envelope.Session
		return d, nil
	default:
		return nil, fmt.Errorf("envelope type %s cannot be processed", envelope.Type.Type)
	}
}

func send(b driver.Batch, cnt int64) {
	if cnt > 0 {
		if err := b.Send(); err != nil {
			log.WithError(err).Warn("problems sending batch")
		}
	} else {
		if err := b.Abort(); err != nil {
			log.WithError(err).Warn("problems aborting batch")
		}
	}
}

func buildEventComponents(v1 *ErrorTrackingErrorEvent) []interface{} {
	return []interface{}{
		v1.ProjectID,
		v1.Fingerprint,
		v1.Name,
		v1.Description,
		v1.Actor,
		v1.Environment,
		v1.Platform,
		v1.Level,
		v1.UserIdentifier,
		v1.Payload,
		v1.OccurredAt,
	}
}

func buildStatusComponents(v1 *ErrorTrackingErrorEvent) []interface{} {
	return []interface{}{
		v1.ProjectID,
		v1.Fingerprint,
		uint8(errorUnresolved),
		uint64(0),
		uint8(2),
		time.Now(),
	}
}

func buildEventV2Components(v2 *ErrorEventV2) []interface{} {
	return []interface{}{
		v2.EventID,
		v2.ProjectID,
		v2.Timestamp,
		v2.IsDeleted,
		string(v2.Fingerprint),
		v2.Actor,
		v2.Platform,
		v2.Environment,
		v2.Level,
		v2.Payload,
		v2.Message,
		v2.Transaction,
	}
}

func buildStatusV2Components(v2 *ErrorEventV2) []interface{} {
	return []interface{}{
		v2.EventID,
		v2.ProjectID,
		v2.Timestamp,
		v2.IsDeleted,
		string(v2.Fingerprint),
		uint8(errorUnresolved),
		v2.Actor,
		uint8(2),
		time.Now(),
	}
}

func buildMessageCompnents(e *ErrorTrackingMessageEvent) []interface{} {
	return []interface{}{
		e.EventID,
		e.ProjectID,
		e.Timestamp,
		e.IsDeleted,
		string(e.Fingerprint),
		e.Environment,
		e.Level,
		e.Message,
		e.Actor,
		e.Platform,
		e.Release,
		e.ServerName,
		e.Payload,
	}
}

func buildSessionsCompnents(e *ErrorTrackingSession) []interface{} {
	return []interface{}{
		e.ProjectID,
		e.SessionID,
		e.UserID,
		e.Init,
		e.Payload,
		e.Started,
		e.OccurredAt,
		e.Duration,
		e.Status,
		e.Release,
		e.Environment,
	}
}
