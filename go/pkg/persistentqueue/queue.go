package persistentqueue

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/xujiajun/nutsdb"
	"github.com/xujiajun/nutsdb/ds/list"
)

const (
	defaultQueueName  string = "default"
	defaultBucketName string = "default"
)

// DefaultQueueProcessor just prints data passed in `buffer` to
// STDOUT, helps debugging when writing logic around a Queue
// instance.
var DefaultQueueProcessor = func(buffer []interface{}) error {
	for _, d := range buffer {
		fmt.Printf("%v", d)
	}
	return nil
}

type Queue struct {
	dirPath        string
	name           string
	batchSize      int
	processor      func([]interface{}) error
	processorCount int

	queueDB *nutsdb.DB
	wg      sync.WaitGroup

	// mu guards ingesterClosed
	mu             sync.RWMutex
	ingesterClosed bool
}

type QueueOption func(*Queue)

func WithDirPath(dirPath string) QueueOption {
	return func(b *Queue) {
		b.dirPath = dirPath
	}
}

func WithQueueName(name string) QueueOption {
	return func(b *Queue) {
		b.name = name
	}
}

func WithBatchSize(size int) QueueOption {
	return func(b *Queue) {
		b.batchSize = size
	}
}

func WithProcessor(p func([]interface{}) error) QueueOption {
	return func(b *Queue) {
		b.processor = p
	}
}

func WithProcessorCount(p int) QueueOption {
	return func(b *Queue) {
		b.processorCount = p
	}
}

func NewQueue(options ...QueueOption) (*Queue, error) {
	queue := &Queue{}
	for _, opt := range options {
		opt(queue)
	}
	queue.setupDefaults()

	if err := os.MkdirAll(queue.dirPath, 0644); err != nil {
		return nil, fmt.Errorf("error creating requested directory path: %w", err)
	}

	opts := nutsdb.DefaultOptions
	db, err := nutsdb.Open(opts, nutsdb.WithDir(queue.dirPath))
	if err != nil {
		return nil, fmt.Errorf("creating a nutsdb instance: %w", err)
	}
	queue.queueDB = db

	// spawn queue.processorCount digesters
	queue.wg.Add(queue.processorCount)
	for i := 0; i < queue.processorCount; i++ {
		go queue.digester()
	}

	return queue, nil
}

// compile-time assertion that Queue does satisfy the interface at all times.
var _ IQueue = (*Queue)(nil)

type IQueue interface {
	Close()
	Insert(v []byte) error
	SetProcessor(processor func(b []interface{}) error)
}

func (b *Queue) Close() {
	b.mu.Lock()
	b.ingesterClosed = true // mark the ingester closed
	b.mu.Unlock()

	b.wg.Wait() // wait for ingesters/digesters to have completed
}

func (b *Queue) setupDefaults() {
	b.ingesterClosed = false

	if b.name == "" {
		b.name = defaultQueueName
	}
	if b.batchSize == 0 {
		b.batchSize = 1
	}
	if b.processor == nil {
		b.processor = DefaultQueueProcessor
	}
	if b.processorCount == 0 {
		b.processorCount = 1
	}
}

func (b *Queue) Insert(v []byte) error {
	if b.ingesterClosed {
		return fmt.Errorf("queue closed")
	}
	if err := b.queueDB.Update(
		func(tx *nutsdb.Tx) error {
			//nolint:wrapcheck
			return tx.RPush(defaultBucketName, []byte(b.name), v)
		},
	); err != nil {
		return fmt.Errorf("queue insert: %w", err)
	}
	return nil
}

func (b *Queue) SetProcessor(processor func(b []interface{}) error) {
	b.processor = processor
}

func (b *Queue) digester() {
	computeQueueLength := func() (int, error) {
		var computedLength int
		err := b.queueDB.Update(
			func(tx *nutsdb.Tx) error {
				size, err := tx.LSize(defaultBucketName, []byte(b.name))
				if err != nil {
					return err //nolint:wrapcheck
				}
				computedLength = size
				return nil
			},
		)
		if err != nil {
			return -1, err //nolint:wrapcheck
		}
		return computedLength, nil
	}

	defer b.wg.Done()
	for {
		// check if the queue is empty and the ingester has been closed
		size, err := computeQueueLength()
		if err != nil {
			// this can happen when a given bucket is unknown to the hint index,
			// since we run multiple replicas of this processor, unless there's
			// sufficient incoming data, not all queues will be initialized with
			// data together.
			if !errors.Is(err, nutsdb.ErrBucket) {
				log.Print(fmt.Errorf("computing queue length: %w", err))
			}
			continue
		}

		b.mu.RLock()
		if size == 0 && b.ingesterClosed {
			b.mu.RUnlock()
			return // we're done here
		}
		b.mu.RUnlock()

		// keep building a batch of enqueued data otherwise
		var items []interface{}
		for i := 0; i < b.batchSize; i++ {
			err := b.queueDB.Update(
				func(tx *nutsdb.Tx) error {
					item, err := tx.LPop(defaultBucketName, []byte(b.name))
					if err != nil {
						return err //nolint:wrapcheck
					}
					// fmt.Println("LPop item:", string(item))
					items = append(items, item)
					return nil
				},
			)
			if err != nil {
				if !errors.Is(err, list.ErrListNotFound) {
					log.Print(fmt.Errorf("dequeueing from queue: %w", err))
				}
			}
		}
		// send accumulated data to the processor
		if len(items) > 0 {
			if err := b.processor(items); err != nil {
				log.Print(err)
			}
		}
	}
}
