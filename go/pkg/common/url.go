package common

import "net/url"

// MustParse parses a URL and panics if error occurs.
func MustParse(in string) *url.URL {
	out, err := url.Parse(in)
	if err != nil {
		panic(err)
	}
	return out
}
