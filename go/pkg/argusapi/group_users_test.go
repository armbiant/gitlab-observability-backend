package argusapi

import (
	"testing"

	"github.com/gobs/pretty"
)

const (
	getGroupUsersJSON   = `[{"groupID":1,"userID":1,"email":"admin@localhost","avatarUrl":"/avatar/46d229b033af06a191ff2267bca9ae56","login":"admin","role":"Admin","lastSeenAt":"2018-06-28T14:16:11Z","lastSeenAtAge":"\u003c 1m"}]`
	addGroupUserJSON    = `{"message":"User added to group"}`
	updateGroupUserJSON = `{"message":"Group user updated"}`
	removeGroupUserJSON = `{"message":"User removed from group"}`
)

func TestGroupUsersCurrent(t *testing.T) {
	server, client := gapiTestTools(t, 200, getGroupUsersJSON)
	defer server.Close()

	resp, err := client.GroupUsersCurrent()
	if err != nil {
		t.Fatal(err)
	}

	user := GroupUser{
		GroupID: 1,
		UserID:  1,
		Email:   "admin@localhost",
		Login:   "admin",
		Role:    "Admin",
	}

	if resp[0] != user {
		t.Error("Not correctly parsing returned group users.")
	}
}

func TestGroupUsers(t *testing.T) {
	server, client := gapiTestTools(t, 200, getGroupUsersJSON)
	defer server.Close()

	group := int64(1)
	resp, err := client.GroupUsers(group)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pretty.PrettyFormat(resp))

	user := GroupUser{
		GroupID: 1,
		UserID:  1,
		Email:   "admin@localhost",
		Login:   "admin",
		Role:    "Admin",
	}

	if resp[0] != user {
		t.Error("Not correctly parsing returned group users.")
	}
}

func TestAddGroupUser(t *testing.T) {
	server, client := gapiTestTools(t, 200, addGroupUserJSON)
	defer server.Close()

	groupID, user, role := int64(1), "admin@localhost", "Admin"

	err := client.AddGroupUser(groupID, user, role)
	if err != nil {
		t.Fatal(err)
	}
}

func TestUpdateGroupUser(t *testing.T) {
	server, client := gapiTestTools(t, 200, updateGroupUserJSON)
	defer server.Close()

	groupID, userID, role := int64(1), int64(1), "Editor"

	err := client.UpdateGroupUser(groupID, userID, role)
	if err != nil {
		t.Fatal(err)
	}
}

func TestRemoveGroupUser(t *testing.T) {
	server, client := gapiTestTools(t, 200, removeGroupUserJSON)
	defer server.Close()

	groupID, userID := int64(1), int64(1)

	err := client.RemoveGroupUser(groupID, userID)
	if err != nil {
		t.Error(err)
	}
}
