package gcp

import (
	"context"
	"fmt"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/dns/v1"
	"google.golang.org/api/option"
)

const (
	RecordTypeNS = "NS"
)

type Provider struct {
	project string
	client  *dns.Service
}

func NewDNSProvider(ctx context.Context, projectName string) (*Provider, error) {
	if projectName == "" {
		return nil, fmt.Errorf("GCP project name missing")
	}

	client, err := google.DefaultClient(ctx, dns.NdevClouddnsReadwriteScope)
	if err != nil {
		return nil, fmt.Errorf("unable to get GCP client: %w", err)
	}

	svc, err := dns.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		return nil, fmt.Errorf("unable to create GCP DNS service: %w", err)
	}

	return &Provider{
		project: projectName,
		client:  svc,
	}, nil
}

func (p *Provider) ListManagedZones(ctx context.Context, project string) (map[string]*dns.ManagedZone, error) {
	zones := make(map[string]*dns.ManagedZone)
	err := p.client.ManagedZones.List(project).Pages(
		ctx,
		func(resp *dns.ManagedZonesListResponse) error {
			for _, zone := range resp.ManagedZones {
				zones[zone.Name] = zone
			}
			return nil
		},
	)
	if err != nil {
		return nil, err
	}
	return zones, nil
}

func (p *Provider) CreateManagedZone(project string, managedzone *dns.ManagedZone) (*dns.ManagedZone, error) {
	return p.client.ManagedZones.Create(project, managedzone).Do()
}

func (p *Provider) CreateRecords(project string, managedZone string, change *dns.Change) (*dns.Change, error) {
	return p.client.Changes.Create(project, managedZone, change).Do()
}
