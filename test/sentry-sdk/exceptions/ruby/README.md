# error-tracking-test Ruby

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
bundle install
ruby app.rb
```

