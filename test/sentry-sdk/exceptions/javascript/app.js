
const Sentry = require('@sentry/node');

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true
});

Sentry.captureException(new Error('Hello from Sentry Javascript SDK'));
