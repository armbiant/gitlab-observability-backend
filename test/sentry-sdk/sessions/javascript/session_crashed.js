
const Sentry = require('@sentry/node');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  debug: true,
  release:"my-javascriprt-project@1.0.0",
  environment: "dev"
});

Sentry.captureException(new Error('Hello from Sentry Javascript SDK'));

console.log("Hello");
// This will generate a session with status = "crashed" since we are not capturing the error
// and this is the reason why the application exits. The application exits with a non 0 exit code.
// If we remove the following lines then we will receive a session with status = "exited" since
// the application will exit without an error.
setTimeout(function () {
  throw new Error('We crashed!!!!!');
}, 2);