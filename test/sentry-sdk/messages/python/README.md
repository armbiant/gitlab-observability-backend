# error-tracking-test Python - Message Interface

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
python3 app.py
```

The payload that is emitted with `capture_message` is being sent to the Store endpoint and looks like the following:
```
{
    "message": "Testing capture_message in python",
    "level": "info",
    "event_id": "1ecfd051fb284c46a390db9e8ba15dfe",
    "timestamp": "2023-01-12T09:35:34.213717Z",
    "breadcrumbs": {
        "values": []
    },
    "transaction_info": {},
    "contexts": {
        "runtime": {
            "name": "CPython",
            "version": "3.11.1",
            "build": "3.11.1 (v3.11.1:a7a450f84a, Dec  6 2022, 15:24:06) [Clang 13.0.0 (clang-1300.0.29.30)]"
        }
    },
    "modules": {
        "certifi": "2022.6.15",
        "setuptools": "65.5.0",
        "pip": "22.3.1",
        "urllib3": "1.26.11",
        "sentry-sdk": "1.9.0"
    },
    "extra": {
        "sys.argv": [
            "../python/app.py"
        ]
    },
    "release": "v1.0.0",
    "environment": "dev",
    "server_name": "Nicks-MacBook-Pro.local",
    "sdk": {
        "name": "sentry.python",
        "version": "1.9.0",
        "packages": [
            {
                "name": "pypi:sentry-sdk",
                "version": "1.9.0"
            }
        ],
        "integrations": [
            "argv",
            "atexit",
            "dedupe",
            "excepthook",
            "logging",
            "modules",
            "redis",
            "stdlib",
            "threading"
        ]
    },
    "platform": "python"
}
```

with `attach_stacktrace` to true:
```
{
    "message": "Testing capture_message in python",
    "level": "info",
    "event_id": "7979761fb8a24a9c81633f1c92c55d7b",
    "timestamp": "2023-01-12T10:08:37.814591Z",
    "breadcrumbs": {
        "values": []
    },
    "transaction_info": {},
    "contexts": {
        "runtime": {
            "name": "CPython",
            "version": "3.11.1",
            "build": "3.11.1 (v3.11.1:a7a450f84a, Dec  6 2022, 15:24:06) [Clang 13.0.0 (clang-1300.0.29.30)]"
        }
    },
    "modules": {
        "certifi": "2022.6.15",
        "setuptools": "65.5.0",
        "pip": "22.3.1",
        "urllib3": "1.26.11",
        "sentry-sdk": "1.9.0"
    },
    "extra": {
        "sys.argv": [
            "../python/app.py"
        ]
    },
    "threads": {
        "values": [
            {
                "stacktrace": {
                    "frames": [
                        {
                            "filename": "app.py",
                            "abs_path": "/Users/nickilieskou/projects/error-tracking-test/messages/python/app.py",
                            "function": "<module>",
                            "module": "__main__",
                            "lineno": 29,
                            "pre_context": [
                                "    if dsn is None:",
                                "        print("empty dsn; exiting")",
                                "        sys.exit(1)",
                                "",
                                "    init_sentry(dsn)"
                            ],
                            "context_line": "    sentry_sdk.capture_message("Testing capture_message in python")",
                            "post_context": [
                                "",
                                ""
                            ],
                            "vars": {
                                "__name__": "'__main__'",
                                "__doc__": "None",
                                "__package__": "None",
                                "__loader__": "<_frozen_importlib_external.SourceFileLoader object at 0x102fc5350>",
                                "__spec__": "None",
                                "__annotations__": {},
                                "__builtins__": "<module 'builtins' (built-in)>",
                                "__file__": "'/Users/nickilieskou/projects/error-tracking-test/messages/go/../python/app.py'",
                                "__cached__": "None",
                                "sentry_sdk": "<module 'sentry_sdk' from '/Library/Frameworks/Python.framework/Versions/3.11/lib/python3.11/site-packages/sentry_sdk/__init__.py'>"
                            },
                            "in_app": true
                        }
                    ]
                },
                "crashed": false,
                "current": true
            }
        ]
    },
    "release": "v1.0.0",
    "environment": "dev",
    "server_name": "Nicks-MacBook-Pro.local",
    "sdk": {
        "name": "sentry.python",
        "version": "1.9.0",
        "packages": [
            {
                "name": "pypi:sentry-sdk",
                "version": "1.9.0"
            }
        ],
        "integrations": [
            "argv",
            "atexit",
            "dedupe",
            "excepthook",
            "logging",
            "modules",
            "redis",
            "stdlib",
            "threading"
        ]
    },
    "platform": "python",
    "_meta": {
        "threads": {
            "values": {
                "0": {
                    "stacktrace": {
                        "frames": {
                            "0": {
                                "vars": {
                                    "": {
                                        "len": 15
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
```