#!/usr/bin/env python
import sentry_sdk, os, sys

def init_sentry(dsn: str) -> None:

    sentry_sdk.init(
        dsn=dsn,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        release="v1.0.0",
        environment="dev",
        traces_sample_rate=1.0,
        attach_stacktrace=True,
	debug=True,
    )


def failing_function() -> None:
    raise Exception("An exception")

if __name__ == "__main__":
    dsn = os.getenv("SENTRY_DSN")
    if dsn is None:
        print("empty dsn; exiting")
        sys.exit(1)

    init_sentry(dsn)
    sentry_sdk.capture_message("Testing capture_message in python")


