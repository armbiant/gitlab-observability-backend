require "sentry-ruby"



Sentry.init do |config|
  config.dsn =  ENV["SENTRY_DSN"]
  config.debug = true
  config.release = "v1.0.0"
  config.environment = "dev"
  config.breadcrumbs_logger = [:sentry_logger, :http_logger]
end


Sentry.capture_message("Testing capture_message fropm Ruby Sentry SDK")
