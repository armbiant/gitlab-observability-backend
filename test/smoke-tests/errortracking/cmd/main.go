package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	errortracking "gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/applogger"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/http_client"
)

const (
	metricsPort = 2112
)

var (
	appLog *logrus.Entry
)

type ComponentUptimeTest interface {
	RunUptimeTest() error
}

func init() {
	appLog = applogger.Get()

	err := config.InitializeConfig()
	if err != nil {
		appLog.Fatal(err)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	go func() {
		appLog.Info("metrics server starting")
		srv := &http.Server{
			Addr:              fmt.Sprintf("0.0.0.0:%d", metricsPort),
			Handler:           mux,
			ReadHeaderTimeout: time.Second * 3,
		}
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				appLog.Fatal("unable to start metric server")
			}
		}
	}()

	et := errortracking.ErrorTrackingTestRunner{
		SentryClient: &errortracking.SentryClientWrapper{},
		HttpClient:   &http_client.HttpRequestWrapper{},
	}

	retries := config.GetRetries()
	for {
		err := et.RunUptimeTest()
		if err != nil {
			appLog.Errorf("--- Error-Tracking read and write path - FAIL = %v ---", err)
			retries -= 1
			if retries <= 0 {
				appLog.Errorf("Reached max retries %d. Exiting...", config.GetRetries())
				os.Exit(1)
			} else {
				appLog.Infof("Retrying. Remaining attempts: %d", retries)
			}
		} else {
			appLog.Infof("--- Error-Tracking read and write path - PASS ---")
			retries = config.GetRetries()
		}
		time.Sleep(time.Duration(config.GetTestsPeriodInSec()) * time.Second)
	}
}
