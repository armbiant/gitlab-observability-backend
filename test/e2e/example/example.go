package example

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/framework"
)

var _ = Describe("example component", Ordered, Serial, func() {
	Context("when exampling", func() {
		BeforeAll(func() {
			f, err := framework.GetInstance()
			Expect(err).ToNot(HaveOccurred())
			Expect(f).ToNot(BeNil())
			// k8sClient, err := f.GetK8sClient()
		})

		It("does something here", func() {
			Expect(true).To(BeTrue())
		})
	})
})
