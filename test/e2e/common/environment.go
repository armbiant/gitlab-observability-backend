package common

import (
	"fmt"
	"os"
	"strings"
)

const (
	GitlabDomainNameTmpl   string = "test-%s-gitlab.opstracegcp.com"
	PlatformDomainNameTmpl string = "test-%s-platform.opstracegcp.com"
)

// GCP-specific project(s)
const (
	DevelopmentProjectName string = "vast-pad-240918"
	DevRealmProjectName    string = "opstrace-dev-bee41fca"
	RootManagedZoneName    string = "root-opstracegcp"
)

var envVars map[string]string

func init() {
	envVars = make(map[string]string)
}

func AddEnvVar(k, v string) {
	envVars[k] = v
}

func DeleteEnvVar(k string) {
	if _, ok := envVars[k]; !ok {
		return
	}
	delete(envVars, k)
}

func GetEnvVars() map[string]string {
	return envVars
}

// SetupEnvVars sets up all environment variables, which is then
// consumed by the entire test suite, all Terraform/Terragrunt
// workflows included. It also helps perform any validation necessary
// to make sure we have all variables configured right at the start
// of the test run. It's invoked when the test suite is first setup
// inside tests/e2e/e2e.go.
func SetupEnvVars(testIdentifier string) error {
	if _, ok := os.LookupEnv("TEST_ENV_GITLAB_IMAGE"); !ok {
		return fmt.Errorf("TEST_ENV_GITLAB_IMAGE not set")
	}
	gitlabImage := os.Getenv("TEST_ENV_GITLAB_IMAGE")

	if _, ok := os.LookupEnv("TF_VAR_registry_username"); !ok {
		return fmt.Errorf("TF_VAR_registry_username not set")
	}
	registryUsername := os.Getenv("TF_VAR_registry_username")

	if _, ok := os.LookupEnv("TF_VAR_registry_username"); !ok {
		return fmt.Errorf("TF_VAR_registry_auth_token not set")
	}
	registryAuthToken := os.Getenv("TF_VAR_registry_auth_token")

	// unless overridden, scheduler image used is inferred from the test identifier
	var schedulerImage string
	if _, ok := os.LookupEnv("TEST_ENV_SCHEDULER_IMAGE"); ok {
		schedulerImage = os.Getenv("TEST_ENV_SCHEDULER_IMAGE")
	} else {
		schedulerImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace/scheduler:0.1.44"
	}

	var gouiImage string
	if _, ok := os.LookupEnv("TEST_ENV_GOUI_IMAGE"); ok {
		gouiImage = os.Getenv("TEST_ENV_GOUI_IMAGE")
	} else {
		gouiImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:bac2ebe8"
	}

	gitlabDomain := fmt.Sprintf(GitlabDomainNameTmpl, testIdentifier)
	cloudDNSZone := strings.ReplaceAll(gitlabDomain, ".", "-")

	// setup envVars
	envVars = map[string]string{
		"TF_VAR_bucket_name":              "terragrunt-e2e-ci-pipelines",
		"TF_VAR_remote_state_prefix":      testIdentifier,
		"TF_VAR_project_id":               DevRealmProjectName,
		"TF_VAR_instance_name":            fmt.Sprintf("testplatform-%s", testIdentifier),
		"TF_VAR_region":                   "us-west2",
		"TF_VAR_location":                 "us-west2-a",
		"TF_VAR_zone":                     "us-west2-a",
		"TF_VAR_opstrace_domain":          fmt.Sprintf(PlatformDomainNameTmpl, testIdentifier),
		"TF_VAR_gitlab_domain":            gitlabDomain,
		"TF_VAR_cloud_dns_zone":           cloudDNSZone,
		"TF_VAR_acme_server":              "",
		"TF_VAR_acme_email":               "abhatnagar@gitlab.com",
		"TF_VAR_cert_issuer":              "letsencrypt-prod",
		"TF_VAR_domain":                   gitlabDomain,
		"TF_VAR_gitlab_image":             gitlabImage,
		"TF_VAR_cluster_secret_name":      "auth-secret",
		"TF_VAR_cluster_secret_namespace": "default",
		"TF_VAR_scheduler_image":          schedulerImage,
		"TF_VAR_goui_image_path":          gouiImage,
		"TF_VAR_registry_username":        registryUsername,
		"TF_VAR_registry_auth_token":      registryAuthToken,
	}
	return nil
}
