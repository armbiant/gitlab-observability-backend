package namespace

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/framework"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	targetNamespaceID   = 9970
	targetNamespaceName = "gitlab-org"
	tenantName          = "opstrace-tenant"
)

var _ = Describe("namespace provisioning", Ordered, Serial, func() {
	var (
		ctx            context.Context
		testIdentifier string
		k8sClient      client.Client
		defaultClient  client.Client
	)

	cluster := &schedulerv1alpha1.Cluster{}

	BeforeAll(func() {
		f, err := framework.GetInstance()
		Expect(err).ToNot(HaveOccurred())

		ctx, _ = f.GetContextWithCancel()

		k8sClient, err = f.GetK8sClient()
		Expect(err).ToNot(HaveOccurred())

		defaultClient, err = f.GetDefaultClient()
		Expect(err).ToNot(HaveOccurred())

		testIdentifier, err = f.GetTestIdentifier()
		Expect(err).ToNot(HaveOccurred())

		testInfra, err := f.GetTestInfrastructure()
		Expect(err).ToNot(HaveOccurred())

		err = testInfra.GetGOBCluster(testIdentifier, cluster)
		Expect(err).ToNot(HaveOccurred())

		common.CreateOrUpdate(ctx, defaultClient, cluster)
		common.ExpectClusterReady(ctx, defaultClient, cluster)
		common.ExpectClusterPodsReady(ctx, defaultClient)
	})

	AfterAll(func() {
		// delete the cluster CR and underlying resources
		common.DeleteCustomResourceAndVerify(ctx, k8sClient, cluster)
	})

	Context("on creating a GitlabNamespace object", func() {
		It("should reconcile a tenant if needed, a group and related resources", func() {
			gitlabNamespace := &schedulerv1alpha1.GitLabNamespace{
				TypeMeta: metav1.TypeMeta{
					APIVersion: "gitlabnamespaces.opstrace.com/v1alpha1",
					Kind:       "GitlabNamespace",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name: fmt.Sprintf("%d", targetNamespaceID),
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  targetNamespaceID,
					TopLevelNamespaceID: targetNamespaceID,
					Name:                targetNamespaceName,
					Path:                "gitlab-org",
					FullPath:            "gitlab-org",
				},
			}
			// ensure the GitlabNamespace was created successfully
			Expect(k8sClient.Create(ctx, gitlabNamespace)).Should(Succeed())
			// check if the GitlabNamespace exists
			createdGitlabNamespace := &schedulerv1alpha1.GitLabNamespace{}
			common.ExpectObjectExists(
				ctx,
				k8sClient,
				types.NamespacedName{
					Name:      fmt.Sprintf("%d", targetNamespaceID),
					Namespace: fmt.Sprintf("%d", targetNamespaceID),
				},
				createdGitlabNamespace,
			)
			// check if the underlying namespace exists
			common.ExpectObjectExists(
				ctx,
				k8sClient,
				types.NamespacedName{
					Name:      fmt.Sprintf("%d", targetNamespaceID),
					Namespace: fmt.Sprintf("%d", targetNamespaceID),
				},
				&v1.Namespace{},
			)
			// check if the corresponding tenant exists
			createdTenant := &opstracev1alpha1.Tenant{}
			common.ExpectObjectExists(
				ctx,
				k8sClient,
				types.NamespacedName{
					Name:      tenantName,
					Namespace: fmt.Sprintf("%d", targetNamespaceID),
				},
				createdTenant,
			)

			var finalizerAdded bool
			for _, f := range createdTenant.Finalizers {
				if f == "tenant.opstrace.com/finalizer" {
					finalizerAdded = true
					break
				}
			}
			Expect(finalizerAdded).To(BeTrue())
			// expect the GitlabNamespace object to be ready
			common.ExpectGitLabNamespaceReady(ctx, defaultClient, createdGitlabNamespace)
			// expect all pods in the target namespace to be ready
			common.ExpectNamespacePodsReady(ctx, k8sClient, fmt.Sprintf("%d", targetNamespaceID))

			envVars := common.GetEnvVars()
			if _, ok := envVars["TF_VAR_opstrace_domain"]; ok {
				domain := *createdTenant.Spec.Domain
				Expect(domain).To(Equal(envVars["TF_VAR_opstrace_domain"]))
			}
			if _, ok := envVars["TF_VAR_goui_image_path"]; ok {
				image := *createdTenant.Spec.GOUI.Image
				Expect(image).To(Equal(envVars["TF_VAR_goui_image_path"]))
			}
		})
	})

	Context("on deleting a GitlabNamespace object", func() {
		It("should reconcile the tenant, delete the group and related resources", func() {
			// delete the GitlabNamespace object
			gitlabNamespace := &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      fmt.Sprintf("%d", targetNamespaceID),
					Namespace: fmt.Sprintf("%d", targetNamespaceID),
				},
			}
			Expect(k8sClient.Delete(ctx, gitlabNamespace)).To(Succeed())

			// ensure the GitlabNamespace object gets deleted eventually
			gitlabNamespaceLookupKey := types.NamespacedName{
				Name:      fmt.Sprintf("%d", targetNamespaceID),
				Namespace: fmt.Sprintf("%d", targetNamespaceID),
			}
			readGitlabNamespaceObject := &schedulerv1alpha1.GitLabNamespace{}
			Eventually(func() bool {
				err := k8sClient.Get(ctx, gitlabNamespaceLookupKey, readGitlabNamespaceObject)
				if err != nil && errors.IsNotFound(err) {
					return true
				}
				return false
			}, time.Minute*5, time.Second*10).Should(BeTrue())
		})
	})
})
