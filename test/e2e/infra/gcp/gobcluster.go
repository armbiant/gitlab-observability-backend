package gcp

import (
	"fmt"

	acmev1 "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	testhelpers "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	v1 "k8s.io/api/core/v1"
	"k8s.io/utils/pointer"
)

func getGOBCluster(testIdentifer string, out *schedulerv1alpha1.Cluster) error {
	testhelpers.LoadYaml("../../scheduler/config/examples/Cluster.yaml", out)

	overrides := testhelpers.GetEnvVars()

	out.ObjectMeta.Name = overrides["TF_VAR_instance_name"]

	out.Spec.Target = common.EnvironmentTarget(common.GCP) // do not change

	out.Spec.GOUI.Image = pointer.String(overrides["TF_VAR_goui_image_path"])

	out.Spec.DNS.CertificateIssuer = overrides["TF_VAR_cert_issuer"]
	out.Spec.DNS.Domain = pointer.String(overrides["TF_VAR_opstrace_domain"])
	out.Spec.DNS.ACMEEmail = overrides["TF_VAR_acme_email"]
	out.Spec.DNS.DNS01Challenge = acmev1.ACMEChallengeSolverDNS01{
		CloudDNS: &acmev1.ACMEIssuerDNS01ProviderCloudDNS{
			Project: overrides["TF_VAR_project_id"],
		},
	}

	// e.g. "testplatform-dealk-dns@opstrace-dev-bee41fca.iam.gserviceaccount.com"
	externaldnsServiceAccountName := fmt.Sprintf("%s-dns@%s.iam.gserviceaccount.com",
		overrides["TF_VAR_instance_name"],
		testhelpers.DevRealmProjectName,
	)
	out.Spec.DNS.ExternalDNSProvider = schedulerv1alpha1.ExternalDNSProviderSpec{
		GCP: &schedulerv1alpha1.ExternalDNSGCPSpec{
			DNSServiceAccountName: externaldnsServiceAccountName,
		},
	}
	out.Spec.DNS.FirewallSourceIPsAllowed = []string{"0.0.0.0/0"}

	// e.g. "testplatform-dealk-cm@opstrace-dev-bee41fca.iam.gserviceaccount.com"
	cmServiceAccountName := fmt.Sprintf("%s-cm@%s.iam.gserviceaccount.com",
		overrides["TF_VAR_instance_name"],
		testhelpers.DevRealmProjectName,
	)
	out.Spec.DNS.GCPCertManagerServiceAccount = pointer.String(cmServiceAccountName)

	out.Spec.GitLab.InstanceURL = fmt.Sprintf("https://%s", overrides["TF_VAR_gitlab_domain"])
	out.Spec.GitLab.GroupAllowedAccess = "*"
	out.Spec.GitLab.GroupAllowedSystemAccess = "*"
	out.Spec.GitLab.AuthSecret = v1.LocalObjectReference{
		Name: overrides["TF_VAR_cluster_secret_name"],
	}

	return nil
}
