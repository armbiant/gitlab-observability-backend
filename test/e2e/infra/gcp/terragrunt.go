package gcp

import (
	"fmt"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/testing"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
)

type TerragruntProvisioner struct {
	logger *logger.Logger
}

func NewTerragruntProvisioner(l *logger.Logger) *TerragruntProvisioner {
	if l == nil {
		l = logger.TestingT
	}
	return &TerragruntProvisioner{logger: l}
}

func (p *TerragruntProvisioner) Provision(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         common.GetEnvVars(),
		Logger:          p.logger,
	})

	_, err := terraform.RunTerraformCommandE(t, terraformOptions, terraform.FormatArgs(terraformOptions, "run-all", "init")...)
	if err != nil {
		return err
	}
	_, err = terraform.TgApplyAllE(t, terraformOptions)
	return err
}

func (p *TerragruntProvisioner) Deprovision(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         common.GetEnvVars(),
		Logger:          p.logger,
	})

	_, err := terraform.TgDestroyAllE(t, terraformOptions)
	return err
}

func (p *TerragruntProvisioner) GetProvisionedKubernetesCluster(t testing.TestingT, testIdentifier string) (string, error) {
	if testIdentifier == "" {
		return "", fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration/gke",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         common.GetEnvVars(),
		Logger:          p.logger,
	})

	return terraform.OutputE(t, terraformOptions, "kubeconfig_path")
}
