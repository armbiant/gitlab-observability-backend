package gcp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/testing"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	"golang.org/x/crypto/ssh"

	. "github.com/onsi/ginkgo/v2"
)

type GitLabProvider struct {
	logger *logger.Logger

	publicIP      string
	sshUsername   string
	sshPrivateKey string
	client        *ssh.Client
}

const (
	remoteStatePrefixTmpl string = "%s/gitlab/terraform.tfstate"
)

func NewGitLabProvider(l *logger.Logger) *GitLabProvider {
	if l == nil {
		l = logger.TestingT
	}
	return &GitLabProvider{logger: l}
}

func (g *GitLabProvider) Provision(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	envVars := common.GetEnvVars()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: map[string]interface{}{
			"bucket": envVars["TF_VAR_bucket_name"],
			"prefix": fmt.Sprintf(remoteStatePrefixTmpl, envVars["TF_VAR_remote_state_prefix"]),
		},
		TerraformDir: "../../terraform/environments/integration-gitlab",
		LockTimeout:  "1h",
		EnvVars:      envVars,
		Logger:       g.logger,
		MigrateState: true,
	})

	_, err := terraform.InitAndApplyE(t, terraformOptions)
	return err
}

func (g *GitLabProvider) Deprovision(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	envVars := common.GetEnvVars()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: map[string]interface{}{
			"bucket": envVars["TF_VAR_bucket_name"],
			"prefix": fmt.Sprintf(remoteStatePrefixTmpl, envVars["TF_VAR_remote_state_prefix"]),
		},
		TerraformDir: "../../terraform/environments/integration-gitlab",
		LockTimeout:  "1h",
		EnvVars:      envVars,
		Logger:       g.logger,
	})

	_, err := terraform.DestroyE(t, terraformOptions)
	return err
}

func (g *GitLabProvider) ReadOutput(t testing.TestingT, testIdentifier string, outputKey string) (string, error) {
	if testIdentifier == "" {
		return "", fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	envVars := common.GetEnvVars()
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		BackendConfig: map[string]interface{}{
			"bucket": envVars["TF_VAR_bucket_name"],
			"prefix": fmt.Sprintf(remoteStatePrefixTmpl, envVars["TF_VAR_remote_state_prefix"]),
		},
		TerraformDir: "../../terraform/environments/integration-gitlab",
		LockTimeout:  "1h",
		EnvVars:      envVars,
		Logger:       logger.Discard, // do not log output commands
	})

	output, err := terraform.OutputE(t, terraformOptions, outputKey)
	if err != nil {
		return "", err
	}
	return output, nil
}

func (g *GitLabProvider) SetupSSH(t testing.TestingT, testIdentifier string) error {
	var err error
	g.publicIP, err = g.ReadOutput(GinkgoT(), testIdentifier, "public_ip")
	if err != nil {
		return err
	}
	g.sshUsername, err = g.ReadOutput(GinkgoT(), testIdentifier, "ssh_username")
	if err != nil {
		return err
	}
	g.sshPrivateKey, err = g.ReadOutput(GinkgoT(), testIdentifier, "ssh_private_key")
	if err != nil {
		return err
	}
	signer, err := ssh.ParsePrivateKey([]byte(g.sshPrivateKey))
	if err != nil {
		return err
	}
	sshConfig := &ssh.ClientConfig{
		User:            g.sshUsername,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	g.client, err = ssh.Dial("tcp", fmt.Sprintf("%s:22", g.publicIP), sshConfig)
	if err != nil {
		return err
	}
	return nil
}

func (g *GitLabProvider) runCommand(cmd string) ([]byte, error) {
	s, err := g.client.NewSession()
	if err != nil {
		return nil, err
	}
	defer s.Close()
	return s.CombinedOutput(cmd)
}

func (g *GitLabProvider) InstallGitLab(testIdentifier string) error {
	envVars := common.GetEnvVars()

	// docker login registry.gitlab.com --username $1 --password $2
	cmd := fmt.Sprintf(`
docker login registry.gitlab.com --username %s --password %s`,
		envVars["TF_VAR_registry_username"],
		envVars["TF_VAR_registry_auth_token"],
	)
	output, err := g.runCommand(cmd)
	g.commandLogger(string(output))
	if err != nil {
		return err
	}

	cmd = `docker ps -aqf "name=gitlab"`
	output, err = g.runCommand(cmd)
	g.commandLogger(cmd)
	g.commandLogger(string(output))
	if err != nil {
		return err
	}

	if string(output) == "" {
		// no container running, lets provision one
		cmd = fmt.Sprintf(`
docker run --detach \
--env GITLAB_OMNIBUS_CONFIG="external_url 'https://%s/'; letsencrypt['enable'] = true; letsencrypt['contact_emails'] = ['%s@gitlab.com']" \
--hostname %s \
--publish 443:443 --publish 80:80 \
--name gitlab \
--restart always \
--volume /tmp/gitlab/config:/etc/gitlab \
--volume /tmp/gitlab/logs:/var/log/gitlab \
--volume /tmp/gitlab/data:/var/opt/gitlab \
--shm-size 256m \
%s`,
			envVars["TF_VAR_domain"],
			envVars["TF_VAR_registry_username"],
			envVars["TF_VAR_domain"],
			envVars["TF_VAR_gitlab_image"],
		)
		output, err = g.runCommand(cmd)
		g.commandLogger(cmd)
		g.commandLogger(string(output))
		if err != nil {
			return err
		}
	}
	return nil
}

func (g *GitLabProvider) AwaitGitLabReadiness() error {
	envVars := common.GetEnvVars()
	ticker := time.NewTicker(5 * time.Second)
	for range ticker.C {
		url := fmt.Sprintf("https://%s/users/sign_in", envVars["TF_VAR_domain"])
		request, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return err
		}
		response, err := http.DefaultClient.Do(request)
		if err != nil {
			g.logger.Logf(GinkgoT(), "problems with GET %s: %v", url, err)
		} else {
			if response.StatusCode == 200 {
				g.logger.Logf(GinkgoT(), "connected to %s, instance should be ready", url)
				return nil
			} else {
				g.logger.Logf(GinkgoT(), "waiting for instance to get ready")
			}
		}
	}
	return nil
}

func (g *GitLabProvider) commandLogger(format string, args ...interface{}) {
	g.logger.Logf(GinkgoT(), format, args...)
}

func (g *GitLabProvider) SetupDependencies(testIdentifier string) error {
	envVars := common.GetEnvVars()
	common.AddEnvVar("TF_VAR_internal_endpoint_token", "anystringfornow")

	var cmd string
	cmd = `docker exec -i gitlab grep 'Password:' /etc/gitlab/initial_root_password | sed 's/^.\{10\}//' > /tmp/gitlab_initial_root_password`
	if _, err := g.runCommand(cmd); err != nil {
		return err
	}
	cmd = `cat /tmp/gitlab_initial_root_password`
	output, err := g.runCommand(cmd)
	if err != nil {
		return err
	}
	common.AddEnvVar("TF_VAR_root_password", string(output))

	// Bear in mind the steps are not idempotent by design, since
	// a personal access token cannot be fetched back. We just create
	// a new one and use that one to make requests!
	rootAuthToken := common.RandStringRunes(20)
	common.AddEnvVar("TF_VAR_admin_token", rootAuthToken)

	tokenName := fmt.Sprintf("api-auth-token %s", time.Now().UTC().Format(time.RFC3339))
	cmd = fmt.Sprintf(`
	docker exec -i gitlab gitlab-rails runner \
	"token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: '%s', expires_at: 2.day.from_now); \
	token.set_token('%s'); \
	token.save!"`,
		tokenName,
		rootAuthToken,
	)
	_, err = g.runCommand(cmd)
	if err != nil {
		return err
	}

	// create application
	type body struct {
		Name        string `json:"name"`
		RedirectURI string `json:"redirect_uri"`
		Scopes      string `json:"scopes"`
	}
	postBody := &body{
		Name:        "gitlab-observability",
		RedirectURI: fmt.Sprintf("https://%s/v1/auth/callback", envVars["TF_VAR_opstrace_domain"]),
		Scopes:      "api",
	}
	postBodyJSON, err := json.Marshal(postBody)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("https://%s/api/v4/applications", envVars["TF_VAR_domain"])
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(postBodyJSON))
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", rootAuthToken))
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}
	if response.StatusCode >= 200 && response.StatusCode <= 300 {
		responseAttributes := make(map[string]interface{})
		if err := json.Unmarshal(responseBody, &responseAttributes); err != nil {
			return err
		}
		// setup needed environment variables
		if v, ok := responseAttributes["application_id"].(string); ok {
			common.AddEnvVar("TF_VAR_oauth_client_id", v)
		}
		if v, ok := responseAttributes["secret"].(string); ok {
			common.AddEnvVar("TF_VAR_oauth_client_secret", v)
		}
	}

	return nil
}
