package gcp

import (
	"bufio"
	"fmt"
	"os"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/testing"

	. "github.com/onsi/ginkgo/v2"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
)

type CustomLogger struct { // helps redirect Terratest logs as needed
	writer *bufio.Writer
}

func (l CustomLogger) Logf(t testing.TestingT, format string, args ...interface{}) {
	logger.DoLog(t, 2, l.writer, fmt.Sprintf(format, args...))
	l.writer.Flush() // flush log lines as soon as they're written
}

type GCP struct {
	logfile            *os.File
	logger             *logger.Logger
	clusterProvisioner *TerragruntProvisioner
	gitlabProvider     *GitLabProvider
}

func NewGCP() (*GCP, error) {
	filepath := "/tmp/terragrunt.log"
	if _, ok := os.LookupEnv("TERRAGRUNT_LOG_FILE"); ok {
		filepath = os.Getenv("TERRAGRUNT_LOG_FILE")
	}
	f, err := os.OpenFile(filepath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		return nil, err
	}
	l := logger.New(CustomLogger{writer: bufio.NewWriter(f)})
	gcp := &GCP{
		logfile:            f,
		logger:             l,
		clusterProvisioner: NewTerragruntProvisioner(l),
		gitlabProvider:     NewGitLabProvider(l),
	}
	return gcp, nil
}

func (g *GCP) CreateEnvironment(testIdentifier string) error {
	// check if the credentials have been exported and are available
	if _, ok := os.LookupEnv("DEV_REALM_CREDENTIALS_FILE"); !ok {
		return fmt.Errorf("couldn't find DEV_REALM_CREDENTIALS_FILE from env")
	}
	if _, ok := os.LookupEnv("DEVELOPMENT_CREDENTIALS_FILE"); !ok {
		return fmt.Errorf("couldn't find DEVELOPMENT_CREDENTIALS_FILE from env")
	}
	// setup DNS
	if err := setupDNS(testIdentifier, fmt.Sprintf(common.GitlabDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	if err := setupDNS(testIdentifier, fmt.Sprintf(common.PlatformDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	return nil
}

func (g *GCP) DestroyEnvironment(testIdentifier string) error {
	// destroy DNS setup specific to this test run
	if err := destroyDNS(testIdentifier, fmt.Sprintf(common.GitlabDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	if err := destroyDNS(testIdentifier, fmt.Sprintf(common.PlatformDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	return nil
}

func (g *GCP) CreateGitLabInstance(testIdentifier string) error {
	// setup dev-realm credentials
	if _, ok := os.LookupEnv("DEV_REALM_CREDENTIALS_FILE"); !ok {
		return fmt.Errorf("couldn't find DEV_REALM_CREDENTIALS_FILE from env")
	}
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", os.Getenv("DEV_REALM_CREDENTIALS_FILE")); err != nil {
		return err
	}
	// terragrunt create GCP VM instance
	if err := g.gitlabProvider.Provision(GinkgoT(), testIdentifier); err != nil {
		return err
	}
	// setup SSH for the instance just provisioned
	if err := g.gitlabProvider.SetupSSH(GinkgoT(), testIdentifier); err != nil {
		return err
	}
	// on the instance, install GitLab
	if err := g.gitlabProvider.InstallGitLab(testIdentifier); err != nil {
		return err
	}
	// await GitLab to be ready
	if err := g.gitlabProvider.AwaitGitLabReadiness(); err != nil {
		return err
	}
	// setup dependencies -> fetch root secret, create PAT, create application, export credentials
	if err := g.gitlabProvider.SetupDependencies(testIdentifier); err != nil {
		return err
	}
	return nil
}

func (g *GCP) DestroyGitLabInstance(testIdentifier string) error {
	return g.gitlabProvider.Deprovision(GinkgoT(), testIdentifier)
}

func (g *GCP) CreateK8sCluster(testIdentifier string) (string, error) {
	// setup dev-realm credentials henceforth
	if _, ok := os.LookupEnv("DEV_REALM_CREDENTIALS_FILE"); !ok {
		return "", fmt.Errorf("couldn't find DEV_REALM_CREDENTIALS_FILE from env")
	}
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", os.Getenv("DEV_REALM_CREDENTIALS_FILE")); err != nil {
		return "", err
	}
	// provision the platform specific to this test run
	if err := g.clusterProvisioner.Provision(GinkgoT(), testIdentifier); err != nil {
		return "", err
	}
	// setup access to this cluster
	return g.clusterProvisioner.GetProvisionedKubernetesCluster(GinkgoT(), testIdentifier)
}

func (g *GCP) DestroyK8sCluster(testIdentifier string) error {
	// deprovision the platform provisioned specific to this test run
	if err := g.clusterProvisioner.Deprovision(GinkgoT(), testIdentifier); err != nil {
		return err
	}
	return nil
}

func (g *GCP) GetGOBCluster(testIdentifier string, out *schedulerv1alpha1.Cluster) error {
	return getGOBCluster(testIdentifier, out)
}

func (g *GCP) Close() error {
	// clean up any dependencies
	return g.logfile.Close()
}
