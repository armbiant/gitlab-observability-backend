package e2e

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/framework"
)

var (
	ctx    context.Context
	cancel context.CancelFunc
)

func RunE2ETests(t *testing.T) {
	RegisterFailHandler(Fail)

	SetDefaultEventuallyTimeout(time.Minute * 15)
	SetDefaultEventuallyPollingInterval(time.Second)

	suiteConfig, reporterConfig := GinkgoConfiguration()
	reporterConfig.Verbose = true
	reporterConfig.FullTrace = true
	RunSpecs(t, "E2E Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	// we *must* always initialise the test framework within a
	// BeforeSuite node to allow for all internal dependencies
	// to be created before any of the specs run.
	By("initialising test framework")
	f, err := framework.GetInstance()
	Expect(err).ToNot(HaveOccurred())

	testIdentifier, err := f.GetTestIdentifier()
	Expect(err).ToNot(HaveOccurred())
	By(fmt.Sprintf("running tests for CI_COMMIT_SHORT_SHA=%s", testIdentifier))

	testTarget, err := f.GetTestTarget()
	Expect(err).ToNot(HaveOccurred())
	By(fmt.Sprintf("running tests for TARGET=%s", testTarget))

	By("setting up environment variables")
	err = common.SetupEnvVars(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	testInfra, err := f.GetTestInfrastructure()
	Expect(err).ToNot(HaveOccurred())
	Expect(testInfra).ToNot(BeNil())

	err = testInfra.CreateEnvironment(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	err = testInfra.CreateGitLabInstance(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	kubeconfig, err := testInfra.CreateK8sCluster(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	// configure access to the provisioned cluster, export KUBECONFIG=...
	err = os.Setenv("KUBECONFIG", kubeconfig)
	Expect(err).ToNot(HaveOccurred())
	By(fmt.Sprintf("exporting KUBECONFIG=%s", kubeconfig))

	ctx, cancel = f.GetContextWithCancel()
	By("ensuring we can construct clients for the underlying cluster")
	//
	// This is done inside an `Eventually` block because freshly-minted
	// clusters, esp. inside cloud environments can undergo repairs OR
	// auto-scaling events which causes them to be unreachable for a
	// brief period of time. As soon the clients can be successfully
	// built, we move on downstream.
	//
	Eventually(func() bool {
		err := f.BuildClients()
		return err == nil
	}, 5*time.Minute, 10*time.Second).Should(BeTrue())

	k8sClient, err := f.GetK8sClient()
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	clientset, err := f.GetK8sClientSet()
	Expect(err).NotTo(HaveOccurred())
	Expect(clientset).NotTo(BeNil())

	By("checking that the API server can be consistently queried")
	nsObjectLookupKey := types.NamespacedName{
		Name:      v1.NamespaceDefault,
		Namespace: v1.NamespaceDefault,
	}
	nsObject := &v1.Namespace{}
	Consistently(func() bool {
		if err := k8sClient.Get(ctx, nsObjectLookupKey, nsObject); err != nil {
			log.Println(err)
			return false
		}
		return true
	}, 10*time.Second, 3*time.Second).Should(BeTrue())

	By("checking that the scheduler-manager pod is ready & running")
	Eventually(func() bool {
		pods, err := clientset.CoreV1().Pods(v1.NamespaceDefault).List(
			ctx,
			metav1.ListOptions{LabelSelector: "control-plane=controller-manager"},
		)
		if err != nil {
			log.Println(err)
			return false
		}
		if len(pods.Items) != 1 {
			log.Println(fmt.Errorf("more than one scheduler-manager pod found"))
			return false
		}
		return pods.Items[0].Status.Phase == v1.PodRunning
	}, 10*time.Second, 3*time.Second).Should(BeTrue())

	// if we got to here, the scheduler manager was ready & running stable
})

var _ = AfterSuite(func() {
	defer cancel()

	f, err := framework.GetInstance()
	Expect(err).ToNot(HaveOccurred())

	testIdentifier, err := f.GetTestIdentifier()
	Expect(err).ToNot(HaveOccurred())

	testInfra, err := f.GetTestInfrastructure()
	Expect(err).ToNot(HaveOccurred())

	testInfra.DestroyK8sCluster(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	testInfra.DestroyGitLabInstance(testIdentifier)
	Expect(err).ToNot(HaveOccurred())

	testInfra.DestroyEnvironment(testIdentifier)
	Expect(err).ToNot(HaveOccurred())
})
