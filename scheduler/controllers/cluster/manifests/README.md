This directory contains all the source manifests used by scheduler to launch components.
They are kustomized internally by scheduler during reconciliation to match configuration specified by `Cluster` object.

Each directory MUST (as in RFC sense):
* contain an `UPDATING.md` file that describes how manifests can be updated,
* contain a `kustomization.yaml` file which:
  * performs adjustments to the source manifests that need to be done for all kinds of `Cluster` objects (i.e. base changes),
  * concatenates all resources/manifests into a single YAML file using kustomize's `resources` field,
