package jaegerOperator

import (
	"fmt"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Certificate(cr *v1alpha1.Cluster) *certmanager.Certificate {
	return &certmanager.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      "serving-cert",
			Namespace: cr.Namespace(),
		},
		Spec: certmanager.CertificateSpec{
			DNSNames: []string{
				fmt.Sprintf("%s.%s.svc", getServiceName(), cr.Namespace()),
				fmt.Sprintf("%s.%s.svc.cluster.local", getServiceName(), cr.Namespace()),
			},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "Issuer",
				Name: "selfsigned-issuer",
			},
			SecretName: constants.JaegerOperatorServiceCertName,
			Subject: &certmanager.X509Subject{
				OrganizationalUnits: []string{"jaeger-operator"},
			},
		},
	}
}

func CertificateMutator(cr *v1alpha1.Cluster, current *certmanager.Certificate) {
	cert := Certificate(cr)
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.Subject = cert.Spec.Subject
}
