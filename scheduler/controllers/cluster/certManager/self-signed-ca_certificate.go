package certManager

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func SelfSignedCACertificate(cr *v1alpha1.Cluster) *certmanager.Certificate {
	res := &certmanager.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      constants.SelfSignedCACertName,
			Namespace: cr.Namespace(),
		},
		Spec: certmanager.CertificateSpec{
			IsCA:       true,
			CommonName: constants.SelfSignedCACertName,
			PrivateKey: &certmanager.CertificatePrivateKey{
				Algorithm: certmanager.ECDSAKeyAlgorithm,
				Size:      256,
			},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "ClusterIssuer",
				Name: constants.SelfSignedIssuer,
			},
			SecretName: constants.SelfSignedCACertSecretName,
		},
	}

	return res
}

func SelfSignedCACertificateMutator(cr *v1alpha1.Cluster, current *certmanager.Certificate) {
	cert := SelfSignedCACertificate(cr)
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.PrivateKey = cert.Spec.PrivateKey
	current.Spec.CommonName = cert.Spec.CommonName
	current.Spec.IsCA = cert.Spec.IsCA
}
