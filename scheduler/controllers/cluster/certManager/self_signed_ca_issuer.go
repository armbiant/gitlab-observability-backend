package certManager

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func SelfSignedCAIssuer() *certmanager.ClusterIssuer {
	return &certmanager.ClusterIssuer{
		ObjectMeta: v1.ObjectMeta{
			Name: constants.SelfSignedCAIssuerName,
		},
		Spec: certmanager.IssuerSpec{
			IssuerConfig: certmanager.IssuerConfig{
				CA: &certmanager.CAIssuer{
					SecretName: constants.SelfSignedCACertSecretName,
				},
			},
		},
	}
}

func SelfSignedCAIssuerMutator(current *certmanager.ClusterIssuer) {
	issuer := SelfSignedCAIssuer()
	current.Spec.CA = issuer.Spec.CA
}
