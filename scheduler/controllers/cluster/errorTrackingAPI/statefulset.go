package errortrackingapi

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "4Gi"
	CpuLimit      = "2"
)

var Replicas int32 = 3

func StatefulSet(cr *v1alpha1.Cluster) *v1.StatefulSet {
	return &v1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetErrorTrackingAPIStatefulSetName(),
			Namespace: cr.Namespace(),
		},
	}
}

func StatefulSetSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetErrorTrackingAPIStatefulSetName(),
	}
}

func StatefulSetMutator(
	cr *v1alpha1.Cluster,
	current *v1.StatefulSet,
	clickhouseDSN string,
	apiBaseURL string,
) error {
	currentSpec := &current.Spec
	spec, err := getStatefulSetSpec(cr, currentSpec, clickhouseDSN, apiBaseURL)
	if err != nil {
		return err
	}
	// apply default overrides
	if err := common.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTrackingAPI.Components.StatefulSet.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getStatefulsetAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.StatefulSet.Annotations,
	)
	current.Labels = common.MergeMap(
		getStatefulsetLabels(),
		cr.Spec.Overrides.ErrorTrackingAPI.Components.StatefulSet.Labels,
	)
	return nil
}

func getStatefulsetAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getStatefulsetLabels() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func getPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func GetErrorTrackingAPIStatefulSetName() string {
	return constants.ErrorTrackingAPIName
}

func GetErrorTrackingStatefulSetSelector() map[string]string {
	return map[string]string{
		"app": constants.ErrorTrackingAPIName,
	}
}

func getStatefulSetSpec(
	cr *v1alpha1.Cluster,
	current *v1.StatefulSetSpec,
	clickhouseDSN string,
	apiBaseURL string,
) (v1.StatefulSetSpec, error) {
	containers, err := getStatefulSetContainers(cr, clickhouseDSN, apiBaseURL)
	if err != nil {
		return v1.StatefulSetSpec{}, err
	}

	var (
		terminationGracePeriod int64  = 60
		storageClassName       string = constants.ErrorTrackingAPIQueueStorageClassName
	)

	spec := v1.StatefulSetSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetErrorTrackingStatefulSetSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetErrorTrackingAPIStatefulSetName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:                    containers,
				Volumes:                       getVolumes(),
				ServiceAccountName:            GetErrorTrackingAPIStatefulSetName(),
				TerminationGracePeriodSeconds: &terminationGracePeriod,
				Affinity: common.WithPodAntiAffinity(
					metav1.LabelSelector{
						MatchLabels: GetErrorTrackingStatefulSetSelector(),
					},
					nil,
				),
			},
		},
		VolumeClaimTemplates: []corev1.PersistentVolumeClaim{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: constants.ErrorTrackingAPIQueueVolumeName,
				},
				Spec: corev1.PersistentVolumeClaimSpec{
					StorageClassName: &storageClassName,
					AccessModes: []corev1.PersistentVolumeAccessMode{
						corev1.ReadWriteOnce,
					},
					Resources: corev1.ResourceRequirements{
						Requests: corev1.ResourceList{
							corev1.ResourceStorage: resource.MustParse("1Gi"),
						},
					},
				},
			},
		},
	}

	return spec, nil
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getStatefulSetContainers(cr *v1alpha1.Cluster, clickhouseDSN, apiBaseURL string) ([]corev1.Container, error) {
	containerEnv, err := getStatefulSetContainerEnv(cr, clickhouseDSN, apiBaseURL)
	if err != nil {
		return nil, err
	}

	containers := []corev1.Container{
		{
			Name:  constants.ErrorTrackingAPIName,
			Image: constants.DockerImageFullName(constants.ErrorTrackingImageName),
			Env:   containerEnv,
			Ports: []corev1.ContainerPort{
				{
					Name:          "http",
					ContainerPort: 8080,
				},
				{
					Name:          "metrics",
					ContainerPort: 8081,
				},
			},
			Resources:       getResources(),
			VolumeMounts:    getVolumeMounts(),
			ImagePullPolicy: corev1.PullIfNotPresent,
		},
	}

	return containers, nil
}

func getStatefulSetContainerEnv(cr *v1alpha1.Cluster, clickhouseDSN, apiBaseURL string) ([]corev1.EnvVar, error) {
	gatekeeperURL := fmt.Sprintf("http://gatekeeper.%s.svc.cluster.local:3001", cr.Namespace())
	envVars := []corev1.EnvVar{
		{
			Name:  "API_BASE_URL",
			Value: apiBaseURL,
		},
		{
			Name:  "CLICKHOUSE_DSN",
			Value: clickhouseDSN,
		},
		{
			Name:  "GATEKEEPER_URL",
			Value: gatekeeperURL,
		},
	}

	useRemoteStorage, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return nil, err
	}
	if useRemoteStorage {
		envVars = append(envVars, corev1.EnvVar{
			Name:  "USE_REMOTE_STORAGE",
			Value: string(cr.Spec.Target),
		})
	}

	useClientSideBuffering, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_CLIENT_SIDE_BUFFERING")
	if err != nil {
		return nil, err
	}
	if useClientSideBuffering {
		envVars = append(envVars,
			corev1.EnvVar{
				Name:  "CLIENT_SIDE_BUFFERING_ENABLED",
				Value: "true",
			},
			corev1.EnvVar{
				Name:  "MAX_BATCH_SIZE",
				Value: "1000",
			},
			corev1.EnvVar{
				Name:  "MAX_PROCESSOR_COUNT",
				Value: "1",
			},
		)
	}

	useDBClientCompression, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_DB_CLIENT_COMPRESSION")
	if err != nil {
		return nil, err
	}
	if useDBClientCompression {
		envVars = append(envVars, corev1.EnvVar{
			Name:  "DB_USE_COMPRESSION",
			Value: "true",
		})
	}

	useDebugServer, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_DEBUG_SERVER")
	if err != nil {
		return nil, err
	}
	if useDebugServer {
		envVars = append(envVars, corev1.EnvVar{
			Name:  "DEBUG_ENABLED",
			Value: "true",
		})
	}

	return envVars, nil
}

func getVolumes() []corev1.Volume {
	var volumes []corev1.Volume

	volumes = append(volumes, corev1.Volume{
		Name: constants.ErrorTrackingAPIQueueVolumeName,
		VolumeSource: corev1.VolumeSource{
			PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
				ClaimName: constants.ErrorTrackingAPIQueueDataPVCName,
			},
		},
	})

	return volumes
}

func getVolumeMounts() []corev1.VolumeMount {
	var mounts []corev1.VolumeMount

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ErrorTrackingAPIQueueVolumeName,
		MountPath: constants.ErrorTrackingAPIQueueDataMountPath,
	})

	return mounts
}
