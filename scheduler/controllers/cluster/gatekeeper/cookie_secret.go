package gatekeeper

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Generate random password and don't change it once created
func getSessionCookieData() map[string][]byte {
	return map[string][]byte{
		"COOKIE_SECRET": []byte(utils.RandStringRunes(50)),
	}
}

func SessionCookie(cr *v1alpha1.Cluster) *v1.Secret {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.SessionCookieSecretName,
		Namespace: cr.Namespace(),
	}

	secret.Data = getSessionCookieData()

	return secret
}

// Don't mutate to ensure we don't change the password after
// it's first created
func SessionCookieMutator(cr *v1alpha1.Cluster, current *v1.Secret) error {
	return nil
}
