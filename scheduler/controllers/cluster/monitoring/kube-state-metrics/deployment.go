package kubestatemetrics

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
)

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getDeploymentName(),
			Namespace: cr.Namespace(),
			Labels:    getDeploymentLabels(),
		},
		Spec: getDeploymentSpec(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	current.Name = getDeploymentName()
	currentSpec := current.Spec.DeepCopy()
	spec := getDeploymentSpec()
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Monitoring.KubeStateMetrics.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		current.Annotations,
		getDeploymentAnnotations(),
	)
	current.Annotations = utils.MergeMap(
		current.Annotations,
		cr.Spec.Overrides.Monitoring.KubeStateMetrics.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.Monitoring.KubeStateMetrics.Components.Deployment.Labels,
	)
	return nil
}

func getDeploymentName() string {
	return monitors.KubeStateMetrics
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": monitors.KubeStateMetrics,
	}
}

func getDeploymentAnnotations() map[string]string {
	return map[string]string{}
}

func getDeploymentSpec() v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: pointer.Int32(1),
		Selector: &metav1.LabelSelector{
			MatchLabels: getDeploymentLabels(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: getDeploymentLabels(),
			},
			Spec: corev1.PodSpec{
				Containers: getDeploymentContainers(),
				NodeSelector: map[string]string{
					"kubernetes.io/os": "linux",
				},
				SecurityContext: &corev1.PodSecurityContext{
					RunAsUser:    pointer.Int64(65534),
					RunAsNonRoot: pointer.Bool(true),
				},
				ServiceAccountName: monitors.KubeStateMetrics,
			},
		},
	}
}

func getDeploymentContainers() []corev1.Container {
	return []corev1.Container{
		{
			SecurityContext: &corev1.SecurityContext{
				AllowPrivilegeEscalation: pointer.Bool(false),
				Capabilities: &corev1.Capabilities{
					Drop: []corev1.Capability{
						"ALL",
					},
				},
			},
			Name:  monitors.KubeStateMetrics,
			Image: constants.OpstraceImages().KubeStateMetricsImage,
			Ports: []corev1.ContainerPort{
				{
					ContainerPort: 8080,
					Name:          "http-metrics",
				},
				{
					ContainerPort: 8081,
					Name:          "telemetry",
				},
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("50m"),
					corev1.ResourceMemory: resource.MustParse("100Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("200m"),
					corev1.ResourceMemory: resource.MustParse("150Mi"),
				},
			},
			LivenessProbe: &corev1.Probe{
				ProbeHandler: corev1.ProbeHandler{
					HTTPGet: &corev1.HTTPGetAction{
						Path: "/healthz",
						Port: intstr.FromInt(8080),
					},
				},
				InitialDelaySeconds: 5,
				TimeoutSeconds:      5,
			},
			ReadinessProbe: &corev1.Probe{
				ProbeHandler: corev1.ProbeHandler{
					HTTPGet: &corev1.HTTPGetAction{
						Path: "/",
						Port: intstr.FromInt(8081),
					},
				},
				InitialDelaySeconds: 5,
				TimeoutSeconds:      5,
			},
		},
	}
}
