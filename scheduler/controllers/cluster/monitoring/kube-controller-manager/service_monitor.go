package kubecontrollermanager

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(),
	}
}

func ServiceMonitorMutator(current *monitoring.ServiceMonitor) {
	current.Labels = getServiceMonitorLabels()
	current.Annotations = getServiceMonitorAnnotations()
	current.Spec = getServiceMonitorSpec()
}

func getServiceMonitorName() string {
	return monitors.KubeControllerManager
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"tenant":  "system",
		"k8s-app": monitors.KubeControllerManager,
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec() monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"k8s-app": "kube-controller-manager",
			},
		},
		JobLabel: "k8s-app",
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{"kube-system"},
		},
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Interval: "30s",
			MetricRelabelConfigs: []*monitoring.RelabelConfig{
				{
					Action: "drop",
					Regex:  "etcd_(debugging|disk|request|server).*",
					SourceLabels: []monitoring.LabelName{
						"__name__",
					},
				},
			},
			Port:        "http-metrics",
			HonorLabels: true,
		},
	}
}
