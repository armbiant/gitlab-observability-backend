package apiserver

import (
	"embed"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	prometheus "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/prometheus/helpers"
)

//go:embed apiserver_rules.yaml
//go:embed k8s_rules.yaml
var f embed.FS

func ApiserverPrometheusRule(cr *v1alpha1.Cluster) (*monitoring.PrometheusRule, error) {
	corpus, err := f.ReadFile("apiserver_rules.yaml")
	if err != nil {
		return &monitoring.PrometheusRule{}, err
	}
	return prometheus.BuildPrometheusRule(cr, monitors.APIServer, corpus)
}

func KubernetesPrometheusRule(cr *v1alpha1.Cluster) (*monitoring.PrometheusRule, error) {
	corpus, err := f.ReadFile("k8s_rules.yaml")
	if err != nil {
		return &monitoring.PrometheusRule{}, err
	}
	return prometheus.BuildPrometheusRule(cr, monitors.Kubernetes, corpus)
}

func ApiserverPrometheusRuleMutator(cr *v1alpha1.Cluster, current *monitoring.PrometheusRule) error {
	rule, err := ApiserverPrometheusRule(cr)
	if err != nil {
		return err
	}
	current.Spec = rule.Spec
	return nil
}

func KubernetesPrometheusRuleMutator(cr *v1alpha1.Cluster, current *monitoring.PrometheusRule) error {
	rule, err := KubernetesPrometheusRule(cr)
	if err != nil {
		return err
	}
	current.Spec = rule.Spec
	return nil
}
