package cluster

import (
	"context"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/prometheus"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type PrometheusState struct {
	ServiceMonitor *monitoring.ServiceMonitor
	ServiceAccount *v1.ServiceAccount
	Service        *v1.Service
	Prometheus     *monitoring.Prometheus
}

func NewPrometheusState() *PrometheusState {
	return &PrometheusState{}
}

func (p *PrometheusState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	if err := p.readServiceAccount(ctx, cr, client); err != nil {
		return err
	}
	if err := p.readService(ctx, cr, client); err != nil {
		return err
	}
	if err := p.readPrometheus(ctx, cr, client); err != nil {
		return err
	}
	if err := p.readServiceMonitor(ctx, cr, client); err != nil {
		return err
	}
	return nil
}

func (p *PrometheusState) readServiceAccount(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1.ServiceAccount{}
	selector := prometheus.ServiceAccountSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	p.ServiceAccount = currentState.DeepCopy()
	return nil
}

func (p *PrometheusState) readService(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &v1.Service{}
	selector := prometheus.ServiceSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	p.Service = currentState.DeepCopy()
	return nil
}

func (p *PrometheusState) readPrometheus(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &monitoring.Prometheus{}
	selector := prometheus.PrometheusSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	p.Prometheus = currentState.DeepCopy()
	return nil
}

func (p *PrometheusState) readServiceMonitor(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &monitoring.ServiceMonitor{}
	selector := prometheus.ServiceMonitorSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	p.ServiceMonitor = currentState.DeepCopy()
	return nil
}
