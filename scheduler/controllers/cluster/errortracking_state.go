package cluster

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	errortrackingapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/errorTrackingAPI"
	appsv1 "k8s.io/api/apps/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ErrorTrackingAPIState struct {
	// Track deployment readiness
	Statefulset *appsv1.StatefulSet
	// Track ingress readiness
	Ingress *netv1.Ingress
}

func NewErrorTrackingAPIState() *ErrorTrackingAPIState {
	return &ErrorTrackingAPIState{}
}

func (i *ErrorTrackingAPIState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readStatefulSet(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readIngress(ctx, cr, client)

	return err
}

func (r *ErrorTrackingAPIState) readStatefulSet(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.StatefulSet{}
	selector := errortrackingapi.StatefulSetSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	r.Statefulset = currentState.DeepCopy()
	return nil
}

func (r *ErrorTrackingAPIState) readIngress(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := errortrackingapi.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	r.Ingress = currentState.DeepCopy()
	return nil
}
