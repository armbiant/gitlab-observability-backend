package namespace

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func TestTenant(t *testing.T) {
	domain := "foo.com"
	glURL := "gitlab.com"
	tcp := corev1.ProtocolTCP
	cr := &v1alpha1.GitLabNamespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "test",
		},
		Spec: v1alpha1.GitLabNamespaceSpec{
			ID:                  32,
			TopLevelNamespaceID: 32,
		},
	}
	makeTenant := func(spec tenantOperator.TenantSpec) *tenantOperator.Tenant {
		return &tenantOperator.Tenant{
			ObjectMeta: metav1.ObjectMeta{
				Name:      constants.TenantName,
				Namespace: "32",
				Labels:    map[string]string{},
			},
			Spec: spec,
		}
	}

	tests := []struct {
		name         string
		cluster      *v1alpha1.Cluster
		psqlEndpoint *url.URL
		cr           *v1alpha1.GitLabNamespace
		want         *tenantOperator.Tenant
	}{
		{
			"default setup with domain and gitlab URL",
			&v1alpha1.Cluster{
				Spec: v1alpha1.ClusterSpec{
					DNS: v1alpha1.DNSSpec{
						Domain: &domain,
					},
					GitLab: v1alpha1.GitLabSpec{
						InstanceURL: glURL,
					},
				},
			},
			nil,
			cr,
			makeTenant(tenantOperator.TenantSpec{
				Domain: &domain,
				GOUI: tenantOperator.GOUISpec{
					EgressRules:        getDefaultEgressRules("default"),
					EmbeddingParentURL: &glURL,
				},
			}),
		},
		{
			"kind egress rules added when target is KIND",
			&v1alpha1.Cluster{
				Spec: v1alpha1.ClusterSpec{
					DNS: v1alpha1.DNSSpec{
						Domain: &domain,
					},
					Target: common.KIND,
				},
			},
			nil,
			cr,
			makeTenant(tenantOperator.TenantSpec{
				Domain: &domain,
				GOUI: tenantOperator.GOUISpec{
					EgressRules: append(getDefaultEgressRules("default"), getKINDEgressRules(cr)...),
				},
			}),
		},
		{
			"postgres egress rules with ip and custom port when endpoint set",
			&v1alpha1.Cluster{
				Spec: v1alpha1.ClusterSpec{
					DNS: v1alpha1.DNSSpec{
						Domain: &domain,
					},
				},
			},
			common.MustParse("postgres://foo:bar@192.168.0.10:2345/"),
			cr,
			makeTenant(tenantOperator.TenantSpec{
				Domain: &domain,
				GOUI: tenantOperator.GOUISpec{
					EgressRules: append(getDefaultEgressRules("default"), networkingv1.NetworkPolicyEgressRule{
						Ports: []networkingv1.NetworkPolicyPort{
							{
								Protocol: &tcp,
								Port: func() *intstr.IntOrString {
									p := intstr.FromInt(2345)
									return &p
								}(),
							},
						},
						To: []networkingv1.NetworkPolicyPeer{
							{
								IPBlock: &networkingv1.IPBlock{
									CIDR: "192.168.0.10/32",
								},
							},
						},
					}),
				},
			}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Tenant(tt.psqlEndpoint, tt.cluster, tt.cr)
			assert.Equal(t, tt.want, got)
		})
	}
}
